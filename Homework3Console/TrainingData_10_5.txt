This file contains training data for TrainingData_10_5.txt

Area: 6881.63
Aspect Ratio: 1.73196
Circularity: 0.0822718
Euler Number (4 connectivity): 1
Euler Number (8 connectivity): 1
Perimeter: 1025.24
Spatial Moment ( M(1,0) ) x_direction:  690016
Spatial Moment ( M(0,1) ) y_direction:  1.07646e+006
Bounding box area: 16296
Bounding box perimeter: 530
Symmetry: 0.748282
//center of gravity =
Center of gravity: 100.269 156.426

Normalized area: 0.422289
Normalized perimeter: 1.93441


