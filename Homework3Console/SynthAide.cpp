#include "SynthAide.h"


SynthAide::SynthAide(void){

}


SynthAide::~SynthAide(void){

}

void SynthAide::copy(View* from, View* to, int firstx, int firsty, int width, int height){
	int lastx = firstx+width -1;
	int lasty = firsty+height-1;
	int out[3] = {0, 0, 0};
	for(int y = firsty; y <= lasty; y++) {
	    for(int x = firstx; x <= lastx; x++) {
			from->getSample(x, y, out);
			to->putSample( x, y, out);
	    }
	}
}

double** gaussian(int length) {

	if( length % 2 == 0 )
	    length++;

	// this stddev puts makes a good spread for a given size
	double stddev = length / 4.9;

	// make a 1d gaussian kernel
	double *oned = new double[length];
	for(int i = 0; i < length; i++) {
	    int x = i - length/2;
	    double exponent = x*x / (-2 * stddev * stddev);
	    oned[i] = exp(exponent);
	}

	// make the 2d version based on the 1d
	double **twod = new double*[length];
	for(int i=0; i<length; i++){
		twod[i] = new double[length];
	}
	double sum = 0.0;
	for(int i = 0; i < length; i++) {
	    for(int j = 0; j < length; j++) {
		twod[i][j] = oned[i] * oned[j];
		sum += twod[i][j];
	    }
	}

	// normalize
	for(int i = 0; i < length; i++) {
	    for(int j = 0; j < length; j++) {
		twod[i][j] /= sum;
	    }
	}

	return twod;
}

vector<TwoDLoc*> lessThanEqual(double ** vals, double threshold, int length1, int length2){
	vector<TwoDLoc*> llist;
	for(int r=0; r<length1; r++){
		for(int c=0; c<length2; c++){
			if( vals[r][c] >= 0 && vals[r][c] <= threshold ) {
				llist.push_back( new TwoDLoc(r,c) );
			}
		}
	}

	return llist;
}

void SynthAide::blend(Patch fromPatch, Patch toPatch, int x, int y, double frompart) {
	int tovals[3] = {0, 0, 0};
	toPatch.getSample(x,y, tovals);
	int fromvals[3] = {0,0,0}; 
	fromPatch.getSample(x,y, fromvals);
	int newvals[] = {0,0,0};
	for(int i = 0; i < 3; i++) {
	    double sum = tovals[i]*(1-frompart) + fromvals[i]*frompart;
	    newvals[i] = (int)sum;
	}
	toPatch.putSample(x,y,newvals);
}

int SynthAide::ssd(View *view1, View *view2, int x, int y) {

	int vals[] = {0,0,0};
	view1->getSample(x, y, vals);
	int vals2[] = {0,0,0};
	view2->getSample(x, y, vals2);
	
	int diff = vals[0] - vals2[0];
	int sum = diff * diff;
	diff = vals[1] - vals2[1];
	sum += diff * diff;
	diff = vals[2] - vals2[2];
	sum += diff * diff;

	return sum;
 }









