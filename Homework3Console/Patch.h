#pragma once
#include "View.h"

class Patch:public View{
private:
	int width;
	int height;

public:
	Patch(void);
	~Patch(void);
	Patch(Image *image, int x, int y, int width, int height);

	void setCorner(int x, int y);
	int getWidth(){
		return width;
	}
	int getHeight(){
		return height;
	}

	void getSample(int x, int y, int out[3]);
	void putSample(int x, int y, int values[3]);

	bool rightOnePixel();

	bool nextPixelRow();
	bool nextRow(int overlap);
	bool nextColumn(int overlap);

};

