#pragma once
#include "Image.h"


class View
{
private:
	static bool invalid_indices;

protected:
	Image *image;
	int xoffset, yoffset;

public:
	View(void);
	~View(void);
	
	View(Image* img, int x, int y);

	void setCorner(int x, int y);
	int getCornerX();
	int getCornerY();

	bool isAtLeftEdge();
	bool isAtTopEdge();

	void getSample(int x, int y, int out[3]);

	int getSample(int, int, int);
	void putSample(int, int, int[]);

	void putSample(int, int, int, int);

	Image* getImage(){
		return image;
	}

	int imageX(int x);
	int imageY(int y);
};

