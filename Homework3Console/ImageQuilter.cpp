#include "ImageQuilter.h"


ImageQuilter::ImageQuilter(void){
	patchsize = 36;
	overlapsize = 36;
	inputImageWidth = 185;
	inputImageHeight = 124;
}


ImageQuilter::~ImageQuilter(void){

}

ImageQuilter::ImageQuilter(Image *input, int patch, int overlap, bool allowHor, double pathCost){
	overlapsize = overlap;
	patchsize = patch;
	inputImage = input;
	allowHorizontalPaths = allowHor;
	pathCostWeight = pathCost;
}

Image* ImageQuilter::synthesize(int outwidth, int outheight){
	if(outwidth < patchsize || outheight < patchsize){
		cout<<"Output size is too small"<<endl;
		exit(1);
	}

	int patchcols = (outwidth - patchsize)/(patchsize-overlapsize);
	int patchrows = (outheight - patchsize)/(patchsize-overlapsize);

	int okwidth = patchcols*(patchsize-overlapsize) + patchsize;
	int okheight = patchrows*(patchsize-overlapsize) + patchsize;

	Image *outputImage = new Image(okwidth, okheight, "color");

	int x = rand()%(inputImageWidth - patchsize);
	int y = rand()%(inputImageHeight - patchsize);

	View *inView = new View(inputImage, x, y);
	Patch *outPatch = new Patch(outputImage, 0, 0, patchsize, patchsize);

	SynthAide::copy(inView, outPatch, 0, 0, patchsize, patchsize);

	if( !outPatch->nextColumn(overlapsize) ) return outputImage;
	                             
	// loop over the rows of output patches
	int currow = 0;
	double **dists;
	const int rownum = inputImage->getHeight()-patchsize+1;
	const int colnum = inputImage->getWidth()-patchsize+1;
	dists = new double*[rownum];
	for(int i=0; i<rownum; i++){
		dists[i] = new double[colnum];
	}

	do {

	    // loop over the patches in this row
	    do {

		// get the distances for this neighborhood
		TwoDLoc bestloc = calcDists(dists, outPatch);
		double bestval = dists[bestloc.getRow()][bestloc.getCol()];

		// pick one of the close matches
		double threshold = bestval * 1.1;
		vector<TwoDLoc*> loclist = SynthAide::lessThanEqual(dists, threshold);
		int choice = (int) (rand() % loclist.size());
		TwoDLoc* loc = (TwoDLoc*) loclist[choice];

		// copy in the patch
		//fillAndBlend(outPatch, loc);
		//pathAndFill(outPatch, loc);
		
	    } while( outPatch->nextColumn(overlapsize) );

	    currow++;

	} while( outPatch->nextRow(overlapsize) );

	return outputImage;
    }



    /** This calculates the difference between the path costs of the left
     *  overlap region and the top overlap region.
     */
    double ImageQuilter::getOverlapDistDifference(Patch *outPatch, TwoDLoc loc) {

		Patch *inPatch = new Patch(outPatch, loc.getX(), loc.getY(), patchsize, patchsize);
		double left = 0.0, top = 0.0;
		vector< vector<double> > tolap = getTopOverlapDists(outPatch, inPatch);
		vector< vector<double> > lolap = getLeftOverlapDists(outPatch, inPatch);
		
		for(int r = 0; r < lolap.size(); r++) {
			for(int c = 0; c < lolap[r].size(); c++) {
				top += tolap[r][c];
			left += lolap[r][c];
			}
		}

	return (top > left ? top-left : left-top);
    }



    /** This calculates the distance (SSD) between the overlap part of outPatch
     *  and the corresponding parts of the possible input patches.
     *  If the pathCostWeight extension has been activated, this will also
     *  calculate the path cost and weight the distance based on that cost.
     *  This returns the array index of the smallest distance found.
     *  @param dists This will be filled in. The return value in dists[y][x]
     *               will be the SSD between an input patch with corner
     *               (x,y) and the given output patch.
     */
    TwoDLoc ImageQuilter::calcDists(double **dists, Patch *outPatch) {

	double best = 9999999999999;
	TwoDLoc bestloc;

	// loop over the possible input patch row locations
	Patch *inPatch = new Patch(inputImage, 0, 0, patchsize, patchsize);
	do {
	    // loop over the possible input patch column locations
	    do {

		double sum = 0.0;
		vector< vector<double> > leftoverlap;
		vector< vector<double> > topoverlap;
		int count = 0;

		// handle the left overlap part
		if( ! outPatch->isAtLeftEdge() ) {

		    leftoverlap = getLeftOverlapDists(outPatch, inPatch);
			for(int r = 0; r < leftoverlap.size(); r++) {
			for(int c = 0; c < leftoverlap[r].size(); c++) {
			    sum += leftoverlap[r][c];
			}
		    }
		    count += leftoverlap.size() * leftoverlap[0].size();
		}

		// handle the top overlap part
		if( ! outPatch->isAtTopEdge() ) {

		    topoverlap = getTopOverlapDists(outPatch,inPatch);
			for(int r = 0; r < topoverlap.size(); r++) {
			for(int c = 0; c < topoverlap[r].size(); c++) {
			    sum += topoverlap[r][c];
			}
		    }
		    count += topoverlap.size() * topoverlap[0].size();
		}

		// don't double count the upper left corner;
		if( leftoverlap.size() != 0 && topoverlap.size() != 0 ) {
		    for(int x = 0; x < overlapsize; x++) {
			for(int y = 0; y < overlapsize; y++) {
			    sum -= SynthAide::ssd(outPatch, inPatch, x, y) / 3.0;
			}
		    }
		    count -= overlapsize * overlapsize;
		}

		// make this an average SSD instead
		sum = sum / (count * 255 * 255);

		// do we weight the SSD with the min cost path cost?
		if( pathCostWeight > 0 ) {

		    double cost = avgCostOfBestPath(leftoverlap,topoverlap);

		    // update the sum appropriately
		    cost = cost / (255 * 255);
		    sum = sum * (1-pathCostWeight) + pathCostWeight * cost;
		}

		// save the total and compare to the best yet
		int y = inPatch->getCornerY();
		int x = inPatch->getCornerX();
		dists[y][x] = sum;
		if( sum < best ) {
		    best = sum;
		    bestloc.row = y;
			bestloc.col = x;
		}

	    } while( inPatch->rightOnePixel() );

	} while( inPatch->nextPixelRow() );

	return bestloc;
    }

    /** This returns the cost of the path through the given overlap region
     *  divided by the length of the path.
     */
    double ImageQuilter::avgCostOfBestPath(vector< vector<double> > leftoverlap, vector< vector<double> > topoverlap) {
	double cost;
	int rowcnt;

	if( leftoverlap.size() == 0 ) {
		MinPathFinder *tpath = new MinPathFinder(topoverlap, allowHorizontalPaths);
	    TwoDLoc loc = tpath->bestSourceLoc();
	    cost = tpath->costOf(loc.getRow(), loc.getCol());
	    rowcnt = patchsize;
	}

	else if( topoverlap.size() == 0 ) {

	    MinPathFinder *lpath = new MinPathFinder(leftoverlap, allowHorizontalPaths);
	    TwoDLoc loc = lpath->bestSourceLoc();
	    cost = lpath->costOf(loc.getRow(), loc.getCol());
	    rowcnt = patchsize;
	}

	else {
	    MinPathFinder *lpath = new MinPathFinder(leftoverlap,
						allowHorizontalPaths);
	    MinPathFinder *tpath = new MinPathFinder(topoverlap,
						allowHorizontalPaths);
	    TwoDLoc *lloc = new TwoDLoc(0,0);
	    TwoDLoc *tloc = new TwoDLoc(0,0);
	    choosePathIntersection(lpath, tpath, *lloc, *tloc);
		    
	    // what is the total cost of the two paths?
	    // this ignores the fact that the two have a pt in common
	    cost = lpath->costOf(lloc->getRow(),lloc->getCol());
	    cost += tpath->costOf(tloc->getRow(),tloc->getCol());

	    // what is the combined length of the two paths
	    rowcnt = 2*patchsize-2-lloc->getRow()-tloc->getRow();
	    rowcnt = 2 * patchsize - rowcnt;
	}	

	return cost / rowcnt;
    }

    /** This creates an array the size of the horizontal overlap region
     *  and fills that array with the SSDs between the patches in that
     *  region. The array returned is upside down, such that array[0][0]
     *  is the lower left corner of the overlap region.
     *  (it's that way to be convenient input to MinPathFinder)
     */
    vector< vector<double> > ImageQuilter::getLeftOverlapDists(Patch *outPatch, Patch *inPatch) {
	int rowcnt = outPatch->getHeight();
	vector< vector<double> > dists;
	int arrayr = rowcnt-1;
	for(int r = 0; r < rowcnt; r++) {
	    for(int c = 0; c < overlapsize; c++) {
		dists[arrayr][c] = SynthAide::ssd(outPatch, inPatch, c, r) / 3;
	    }
	    arrayr--;
	}
	return dists;
    }

    /** This creates an array the size of the horizontal overlap region
     *  and fills that array with the SSDs between the patches in that
     *  region. The array is set up to be vertical with one array row
     *  per column of the overlap region and array[0][0] being the
     *  upper right corner of the overlap region.
     */
    vector< vector<double> > ImageQuilter::getTopOverlapDists(Patch *outPatch, Patch *inPatch) {
	// so arrayr = patchwidth-1-patchx  and arrayc = patchy
	int rowcnt = outPatch->getWidth();
	vector< vector<double> > dists;
	for(int patchx = 0; patchx < rowcnt; patchx++) {
	    int arrayr = rowcnt - patchx - 1;
	    for(int patchy = 0; patchy < overlapsize; patchy++) {
		dists[arrayr][patchy] = SynthAide::ssd(outPatch, inPatch,
						      patchx, patchy) / 3;
	    }
	}
	return dists;
    }


    /** This copies a patch from the input image at location loc
     *  into outPatch. The overlap regions will be blended.
     */
    void ImageQuilter::fillAndBlend(Patch *outPatch, TwoDLoc loc) {

	Patch *inPatch = new Patch(inputImage, loc.getX(), loc.getY(),
				  patchsize, patchsize);

	if( outPatch->isAtTopEdge() ) {

	    // blend the overlap area on the left
	    for(int r = 0; r < patchsize; r++) {
		for(int c = 0; c < overlapsize; c++) {
		    double inpart = (double) c / overlapsize;
		    SynthAide::blend(inPatch, outPatch, c, r, inpart);
		}
	    }
	    SynthAide::copy(inPatch, outPatch, overlapsize,
			   0, patchsize-overlapsize, overlapsize);
	}

	else if( outPatch->isAtLeftEdge() ) {
	    
	    // blend the overlap area on top
	    for(int c = 0; c < patchsize; c++) {
		for(int r = 0; r < overlapsize; r++) {
		    double inpart = (double) r / overlapsize;
		    SynthAide::blend(inPatch, outPatch, c, r, inpart);
		}
	    }
	    SynthAide::copy(inPatch, outPatch, 0, overlapsize,
			   overlapsize, patchsize-overlapsize);
	}
	
	else {

	    // blend the overlap area on top
	    for(int c = overlapsize; c < patchsize; c++) {
		for(int r = 0; r < overlapsize; r++) {
		    double inpart = (double) r / overlapsize;
		    SynthAide::blend(inPatch, outPatch, c, r, inpart);
		}
	    }
	    
	    // blend the overlap area on the left
	    for(int r = overlapsize; r < patchsize; r++) {
		for(int c = 0; c < overlapsize; c++) {
		    double inpart = (double) c / overlapsize;
		    SynthAide::blend(inPatch, outPatch, c, r, inpart);
		}
	    }

	    // blend the combined overlap
	    for(int r = 0; r < overlapsize; r++) {
		for(int c = 0; c < overlapsize; c++) {
		    double inpart = (double)c*r/(overlapsize*overlapsize);
		    SynthAide::blend(inPatch, outPatch, c, r, inpart);
		}
	    }
	}

	// copy in the remaining part
	int size = patchsize - overlapsize;
	SynthAide::copy(inPatch, outPatch, overlapsize, overlapsize, size,size);
    }


    /** Uses the min path boundary method to fill in outPatch from the
     *  input image patch at loc.
     */
	void ImageQuilter::pathAndFill(Patch *outPatch, TwoDLoc loc) {

	bool allow = allowHorizontalPaths;
	Patch *inPatch = new Patch(inputImage, loc.getX(), loc.getY(),
				  patchsize, patchsize);

	if( outPatch->isAtLeftEdge() ) {

	    SynthAide::copy(inPatch, outPatch, 0, 0, overlapsize, patchsize);
	    vector< vector<double> > topOverlap = getTopOverlapDists(outPatch, inPatch);
	    MinPathFinder *topFinder = new MinPathFinder(topOverlap, allow);
	    TwoDLoc source = topFinder->bestSourceLoc();
	    followTopOverlapPath(outPatch, inPatch, *topFinder, source);
	}

	else if( outPatch->isAtTopEdge() ) {

	    SynthAide::copy(inPatch, outPatch, 0, 0, patchsize, overlapsize);
	    vector< vector<double> > leftOverlap = getLeftOverlapDists(outPatch, inPatch);
	    MinPathFinder *leftFinder = new MinPathFinder(leftOverlap, allow);
	    TwoDLoc source = leftFinder->bestSourceLoc();
	    followLeftOverlapPath(outPatch, inPatch, *leftFinder, source);
	}

	else {

	    vector< vector<double> > topOverlap = getTopOverlapDists(outPatch, inPatch);
	    vector< vector<double> > leftOverlap = getLeftOverlapDists(outPatch, inPatch);
	    MinPathFinder *topFinder = new MinPathFinder(topOverlap, allow);
	    MinPathFinder *leftFinder = new MinPathFinder(leftOverlap, allow);
	    TwoDLoc *leftloc = new TwoDLoc(0,0);
	    TwoDLoc *toploc = new TwoDLoc(0,0);
	    
	    // find the best combined source
	    choosePathIntersection(leftFinder, topFinder, *leftloc, *toploc);

	    // fill in the corner
	    
	    // first figure out where to take each pixel from
	    vector< vector<bool> > where;

	    // figure out where the left overlap says to take each pixel from
	    while( leftloc->getRow() < overlapsize ) {
		int r = leftloc->getRow();
		for(int c = leftloc->getCol(); c < overlapsize; c++) {
		    where[r][c] = true;
		}
		*leftloc = leftFinder->follow(*leftloc);
	    }

	    // figure out where the top overlap agrees with the left overlap
	    while( toploc->getRow() < overlapsize ) {
		int r = toploc->getRow();
		for(int c = 0; c < overlapsize; c++) {
		    where[c][r] = where[c][r] && c >= toploc->getCol();
		}
		*toploc = topFinder->follow(*toploc);
	    }

	    // fill in the corner for real now
	    for(int r = 0; r < overlapsize; r++) {
		for(int c = 0; c < overlapsize; c++) {
		    if( where[r][c] ) {
				int out[3] = {0,0,0};
				inPatch->getSample(c,r,out);
				outPatch->putSample( c, r, out);
		    }
		}
	    }

	    // handle the rest of the overlap regions
	    followLeftOverlapPath(outPatch, inPatch, *leftFinder, *leftloc);
	    followTopOverlapPath(outPatch, inPatch, *topFinder, *toploc);
	}

		// fill in the non-overlap area
		int size = patchsize - overlapsize;
		SynthAide::copy(inPatch, outPatch, overlapsize, overlapsize, size,size);
	}

		    

    /** Fills in the left overlap area of toPatch using values from
     *  fromPatch while following the path from source in finder.
     */
    void ImageQuilter::followLeftOverlapPath(Patch *toPatch, Patch *fromPatch,
				 MinPathFinder finder, TwoDLoc source) {

	// loop until we reach the destination
	while( source != NULL ) {

	    int y = patchsize - source.getRow() - 1;
	    int x = source.getCol();

	    // values to the right of x are filled in from fromPatch
	    for(x++; x < overlapsize; x++) {
			int out[3] = {0,0,0};
			fromPatch->getSample(x, y, out);
			toPatch->putSample(x, y, out);
	    }

	    // values at that low point are averaged
	    x = source.getCol();
	    SynthAide::blend(fromPatch, toPatch, x, y, 0.5);
	    //int red[] = {255,0,0};  // DEBUG
	    //toPatch.putSample(x, y, red);  // DEBUG

	    // values to the left are untouched

	    // continue to the next row
	    //   we should probably check for this ahead of time and blend
	    //   (instead of replace or ignore) all pixels along the path
	    int oldrow = source.getRow();
	    do{
		source = finder.follow(source);
		} while( source != NULL && source.getRow() == oldrow );
	}

    }

    /** Fills in the top overlap area of toPatch using values from
     *  fromPatch while following the path from source in finder.
     */
    void ImageQuilter::followTopOverlapPath(Patch *toPatch, Patch *fromPatch,
				 MinPathFinder finder, TwoDLoc source) {

	// loop until we reach the destination
	while( source != NULL ) {

	    int x = patchsize - source.getRow() - 1;
	    int y = source.getCol();

	    // values below y are filled in from fromPatch
	    for(y++; y < overlapsize; y++) {
			int out[3] = {0,0,0};
			fromPatch->getSample(x, y, out);
			toPatch->putSample(x, y, out);
	    }

	    // values at that low point are averaged
	    y = source.getCol();
	    SynthAide::blend(fromPatch, toPatch, x, y, 0.5);
	    //int red[] = {255,0,0};  // DEBUG
	    //toPatch.putSample(x, y, red);  // DEBUG

	    // values above are untouched

	    // continue to the next row
	    //   we should probably check for this ahead of time and blend
	    //   (instead of replace or ignore) all pixels along the path
	    int oldrow = source.getRow();
	    do{
		source = finder.follow(source);
		} while( source != NULL && source.getRow() == oldrow );
	}

    }

    /** This finds the intersection of the two given paths.
     *  The intersection point (in each path's coordinates) is put
     *  into the leftloc and toploc params.
     */
    void ImageQuilter::choosePathIntersection(MinPathFinder *leftpath, 
					   MinPathFinder *toppath,
					   TwoDLoc leftloc,
					   TwoDLoc toploc) {

	// find the best combined source
	leftloc.set(patchsize-1,0);          // upper left corner
	toploc.set(patchsize-1,0);           //  of the image
	double bestcost = leftpath->costOf(patchsize-1,0)
	    + toppath->costOf(patchsize-1,0);
	
	for(int y = 0; y < overlapsize; y++) {
	    for(int x = 0; x < overlapsize; x++) {
		double cost = leftpath->costOf(patchsize-1-y,x)
		    +toppath->costOf(patchsize-1-x,y);
		if( bestcost > cost ) {
		    leftloc.set(patchsize-1-y,x);
		    toploc.set(patchsize-1-x,y);
		    bestcost = cost;
		}
	    }
	}
    }
