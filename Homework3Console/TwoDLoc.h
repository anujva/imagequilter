#pragma once
class TwoDLoc {

public:
	int row, col;

	TwoDLoc(){
		row = 0;
		col = 0;
	}

    TwoDLoc(int row, int col) {
		this->row = row;
		this->col = col;
    }

    void set(int row, int col) {
		this->row = row;
		this->col = col;
    }

    int getRow() {
		return row;
    }

    int getCol() {
		return col;
    }

    int getX() {
		return col;
    }

    int getY() {
		return row;
    }
}
