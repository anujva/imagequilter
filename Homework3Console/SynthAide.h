#pragma once
#include<cmath>
#include <vector>
#include "View.h"
#include "Patch.h"
#include "TwoDLoc.h"

class SynthAide{

public:	
	SynthAide(void);
	
	~SynthAide(void);
	
	static void copy(View* from, View* to, int firstx, int firsty, int width, int height);
	static double** gaussian(int length);

	static vector<TwoDLoc*> lessThanEqual(double ** vals, double threshold, int length);

	static void blend(Patch fromPatch, Patch toPatch, int x, int y, double frompart);

	static int ssd(View* view1, View* view2, int x, int y);
};

