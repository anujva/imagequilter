#include "View.h"


View::View(void){

}


View::~View(void){

}

View::View(Image *img, int x, int y){
	image = img;
	setCorner(x,y);
}

void View::setCorner(int x, int y){
	xoffset = x;
	yoffset = y;
}

int View::getCornerX(){
	return xoffset;
}

int View::getCornerY(){
	return yoffset;
}

bool View::isAtLeftEdge(){
	return xoffset == 0;
}

bool View::isAtTopEdge(){
	return yoffset == 0;
}

void View::getSample(int x, int y, int out[3]){
	x = imageX(x);
	y = imageY(y);
	for(int i=0; i<3; i++){
		out[i] = 0;
	}
	out[0] = image->imgcolor[y][x].getR();
	out[1] = image->imgcolor[y][x].getG();
	out[2] = image->imgcolor[y][x].getB();
}

int View::getSample(int channel, int x, int y){
	x = imageX(x);
	y = imageY(y);
	if(channel == 0){
		return image->imgcolor[y][x].getR();
	}else if(channel == 1){
		return image->imgcolor[y][x].getG();
	}else{
		return image->imgcolor[y][x].getB();
	}
}

void View::putSample(int x, int y, int newvals[3]){
	x = imageX(x);
	y = imageY(y);
	image->imgcolor[y][x].setR(newvals[0]);
	image->imgcolor[y][x].setG(newvals[1]);
	image->imgcolor[y][x].setB(newvals[2]);
}

void View::putSample(int channel, int x, int y, int newval){
	x = imageX(x);
	y = imageY(y);

	if(channel == 0){
		image->imgcolor[y][x].setR(newval);
	}else if(channel == 1){
		image->imgcolor[y][x].setG(newval);
	}else{
		image->imgcolor[y][x].setB(newval);
	}
}

int View::imageX(int x){
	return x+xoffset;
}

int View::imageY(int y){
	return y+yoffset;
}