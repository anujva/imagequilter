#include "MinPathFinder.h"


MinPathFinder::MinPathFinder(void){
	
}


MinPathFinder::~MinPathFinder(void){

}

MinPathFinder::MinPathFinder(double **dists, bool allowHorizontal, int distlength1, int distlength2){
	rowcnt = distlength1;
	colcnt = distlength2;
	path = new TwoDLoc*[rowcnt];
	for(int i=0; i<rowcnt; i++){
		path[i] = new TwoDLoc[colcnt];
	}
	costs = new double*[rowcnt];
	for(int i=0; i<rowcnt; i++){
		costs[i] = new double[colcnt];
	}

	if(colcnt == 1){
		for(int r=1; r<rowcnt; r++){
			costs[r][0] = costs[r-1][0] + dists[r][0];
			path[r][0].row = r-1;
			path[r][0].col = 0; 
		}
		return;
	}
	
	for(int r = 0; r < rowcnt-1; r++) {

	    int choice = (costs[r][0] < costs[r][1] ? 0 : 1);
	    costs[r+1][0] = dists[r+1][0] + costs[r][choice];
	    path[r+1][0].row = r;
		path[r+1][0].col = choice;

	    for(int c = 1; c < colcnt-1; c++) {
			choice = (costs[r][c-1] < costs[r][c] ? c-1 : c);
			choice = (costs[r][c+1] < costs[r][choice] ? c+1 : choice);
			costs[r+1][c] = dists[r+1][c] + costs[r][choice];
		
			path[r+1][c].row = r;
			path[r+1][c].col = choice;
		}

	    int c = colcnt-1;
	    choice = (costs[r][c] < costs[r][c-1] ? c : c-1);
	    costs[r+1][c] = dists[r+1][c] + costs[r][choice];
		path[r+1][c].row = r;
		path[r+1][c].col = choice;
	    if( allowHorizontal ) {
			handleHorizontalMovement(dists[r+1], r+1);
	    }

	} 
}

    /** Given a row and column number, this says where to go next to
     *  head toward the destination.
     */
    TwoDLoc MinPathFinder::follow(TwoDLoc currentLoc) {
		return path[currentLoc.getRow()][currentLoc.getCol()];
    }

    /** This returns the loc of the best source column.
     */
	TwoDLoc MinPathFinder::bestSourceLoc() {
		int best = 0;
		for(int i = 1; i < colcnt; i++) {
			if( costs[rowcnt-1][i] < costs[rowcnt-1][best] ) {
			best = i;
			}
		}
		TwoDLoc temp(rowcnt-1,best);
		return temp;
    }

	double MinPathFinder::costOf(int row, int col) {
		return costs[row][col];
    }

	TwoDLoc** MinPathFinder::getPaths() { return path; }
    double** MinPathFinder::getCosts() { return costs; }

    /** This updates costs and path for the given row such that
     *  the path can travel horizontally along the row if it improves
     *  the costs of the paths.
     */
    void MinPathFinder::handleHorizontalMovement(double *dists, int row) {

	bool changed;

	do {
	    changed = false;

	    // handle the left spot
	    int c = 0;
	    int choice = c+1;
	    double newcost = costs[row][choice] + dists[c];
	    if( costs[row][c] > newcost ) {
		changed = true;
		costs[row][c] = newcost;
		path[row][c].row = row;
		path[row][c].col = choice;
	    }

	    // handle the middle spots
	    for(c=1; c < rowcnt -1; c++) {
			choice = (costs[row][c-1] > costs[row][c+1] ? c+1 : c-1);
			newcost = costs[row][choice] + dists[c];
			if( costs[row][c] > newcost ) {
				changed = true;
				costs[row][c] = newcost;
				path[row][c].row = row;
				path[row][c].col = choice;
			}
	    }

		// handle the right column
		c = rowcnt - 1;
		choice = c-1;
		newcost = costs[row][choice] + dists[c];
		if( costs[row][c] > newcost ) {
		changed = true;
		costs[row][c] = newcost;
		path[row][c].row = row;
		path[row][c].col = choice;
	    
		}

	} while( changed );
}



