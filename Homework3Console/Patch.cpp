#include "Patch.h"


Patch::Patch(void){

}


Patch::~Patch(void){

}

Patch::Patch(Image *img, int x, int y, int wid, int hei):View(img, x, y){
	width = wid;
	height = hei;
	setCorner(x, y);
}

void Patch::setCorner(int x, int y){
	if(x+width > image->getWidth() || y+height > image->getHeight() || x< 0 || y<0){
		cout<<"cannot create a patch here"<<endl;
		exit(1);
	}
	View::setCorner(x, y);
}

int Patch::getWidth(){
	return width;
}

int Patch::getHeight(){
	return height;
}

void Patch::getSample(int x, int y, int out[3]){
	if(x>=0 && x<width && y>=0 && y<height){
		View::getSample(x, y, out);
	}else{
		cout<<"Problem with limits"<<endl;
		exit(1);
	}
}

void Patch::putSample(int x, int y, int values[3]){
	if(x>=0 && x<width && y>=0 && y<height){
		View::putSample(x, y, values);
	}else{
		cout<<"Problem with limits"<<endl;
		exit(1);
	}
}

bool Patch::rightOnePixel() {
	if( xoffset+width < image->getWidth() ) {
	    setCorner(xoffset+1, yoffset);
	    return true;
	}
	return false;
}

bool Patch::nextPixelRow() {
	if( yoffset+height < image->getHeight() ) {
	    setCorner(0, yoffset+1);
	    return true;
	}
	return false;
}

bool Patch::nextRow(int overlap) {
	int newy = yoffset + height - overlap;
	if( newy + height > image->getHeight() ) {
	    return false;
	}
	setCorner(0,newy);
	return true;
}

bool Patch::nextColumn(int overlap) {
	int newx = xoffset + width - overlap;
	if( newx + width > image->getWidth() ) {
	    return false;
	}
	setCorner(newx, yoffset);
	return true;
 }