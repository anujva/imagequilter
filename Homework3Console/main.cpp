#include <iostream>
#include <sstream>
#include <fstream>
#include <stack>
#include <list>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include "Image.h"

using namespace std;

class BlockMatching{
public:
	int index_top;
	int index_bottom;
	int index_left;
	int index_right;
};

class SpatialMoment{
public:
	float spatial_moment_x;
	float spatial_moment_y;
};

class Point{
public:
	int i;
	int j;
	Point(){
		i=0;
		j=0;
	}
	Point(int _i, int _j){
		i = _i;
		j = _j;
	}
};

class ImageOCRData{
public:
	float area;
	float perimeter;
	int EulerNumber4;
	int EulerNumber8;
	float circularity;
	SpatialMoment Spatial_moment_second_order;
	float aspect_ratio;
	float bounding_box_area;
	float bounding_box_peri;
	float symmetry_measure;
	Point Ipoints;
	Point Jpoints;

	ImageOCRData(){
		area = 0;
		perimeter = 0;
		EulerNumber4 = 0;
		EulerNumber8 = 0;
		circularity = 0;
		Spatial_moment_second_order.spatial_moment_x = 0;
		Spatial_moment_second_order.spatial_moment_y = 0;
		aspect_ratio = 0;
		bounding_box_area = 0;
		bounding_box_peri = 0;
		symmetry_measure = 0;
		Ipoints.i = 0;
		Ipoints.j = 0;
		Jpoints.i = 0;
		Jpoints.j = 0;
	}

	ImageOCRData(float a, float p, int e4, int e8, float cir, SpatialMoment Spa, float as_r, float bb_ar, float bb_pe, float symm, Point ipoint, Point jpoint){
		area = a;
		perimeter = p;
		EulerNumber4 = e4;
		EulerNumber8 = e8;
		circularity = cir;
		Spatial_moment_second_order = Spa;
		aspect_ratio = as_r;
		bounding_box_area = bb_ar;
		bounding_box_peri = bb_pe;
		symmetry_measure = symm;
		Ipoints = ipoint;
		Jpoints = jpoint;
	} 
};

using namespace std;

//Declaring all functions
Point* mid_point_function(list<Point*> list_of_points, int size_of_list);
int triangleNumber(int, int);
vector< vector<int> > matrix_multiply(vector< vector<int> >, vector< vector<int> >);
Image filtered_image(Image, vector< vector<int> >);
void windowing(Image, float**);
void normalize(float**, float**);
Image binarize(Image);
int count(Image training, int Q[][2]);
ImageOCRData calculations(Image training);
float aspect_ratio(Image training);
float boundingbox_area(Image training);
SpatialMoment Spatial_Moment(Image training);
float boundingbox_perimeter(Image training);
void writeToFile(ImageOCRData Data, string filename);
float symmetry_measure(Image testing);
Point boundingbox_i_values(Image training);
Point boundingbox_j_values(Image training);

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int comparef (const void * a, const void * b)
{
  return ( *(float*)a - *(float*)b );
}

int main(int argc, char* argv[]){
	fstream myfile;
	/*******************************************start of problem 1***********************************************************************
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 1\\piece.raw", ios::in|ios::binary);
	Image piece(300, 300, "color", myfile);
	myfile.close();

	myfile.open("test.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j=0; j<300; j++){
			myfile.put(piece.imgcolor[i][j].getR());
			myfile.put(piece.imgcolor[i][j].getG());
			myfile.put(piece.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<300; j++){
			if(piece.imgcolor[i][j].getB() == 255 && piece.imgcolor[i][j].getG() == 255 && piece.imgcolor[i][j].getR() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}

	cout<<"Top Point 1: "<<endPoint1.j<<"   "<<endPoint1.i<<endl;
	cout<<"Top Point 2: "<<endPoint2.j<<"   "<<endPoint2.i<<endl;
	cout<<"Bottom Point 3: "<<endPoint3.j<<"   "<<endPoint3.i<<endl;
	cout<<"Bottom Point 4: "<<endPoint4.j<<"   "<<endPoint4.i<<endl;

	//Now that we have found the points we can try to compute by how much the image has been rotated.. 
	//we can do that or we can just try to compute the matrix that does rotation, scaling and translation all in one operation
	//From simple matrix multiplication we got the values for the a, b, c, d values of the matrix

	int matrix[][2] = {
		{1.642, 0.2833},
		{-0.2833, 1.642}
	};

	Image fixed_image(120, 120, "color");

	//matrix has been initialized what we will do now is run a 120x120 loop and try to find the approximate value of the pixel from the original image using bilinear
	//interpolation

	for(int i=0; i<120; i++){
		for(int j=0; j<120; j++){
			//calculate the value for the index that we need to pick the value out from the image
			float column = j*1.642+i*0.2833 + 34;
			float row = j*-0.2833+i*1.642 + 68;

			//Now we do the bilinear interpolation
			int x_ceiling = (int)column + 1;
			int x_floor = (int)column;
			int y_ceiling = (int)row + 1;
			int y_floor = (int)row;

			//thresholding of points 
			if(x_ceiling < 0){
				x_ceiling = 0;
			}else if(x_ceiling > 300){
				x_ceiling =300;
			}

			if(x_floor < 0){
				x_floor = 0;
			}else if(x_floor > 300){
				x_floor =300;
			}

			if(y_ceiling < 0){
				y_ceiling = 0;
			}else if(y_ceiling > 300){
				y_ceiling =300;
			}

			if(y_floor < 0){
				y_floor = 0;
			}else if(y_floor > 300){
				y_floor =300;
			}
			//we can now create 4 points of interest
			Point P2(y_floor, x_ceiling);
			Point P1(y_ceiling, x_ceiling);
			Point P4(y_floor, x_floor);
			Point P3(y_ceiling, x_floor);

			//find the value at these points
			int f1R = piece.imgcolor[P1.i][P1.j].getR();
			int f2R = piece.imgcolor[P2.i][P2.j].getR();
			int f3R = piece.imgcolor[P3.i][P3.j].getR();
			int f4R = piece.imgcolor[P4.i][P4.j].getR();

			float factor = (f1R+f2R+f3R+f4R)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1R*(x_floor - column)*(y_floor - row) + f3R*(column - x_ceiling)*(y_floor - row) + f2R*(x_floor-column)*(row - y_ceiling) + f4R*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			fixed_image.imgcolor[i][j].setR(factor);

			int f1G = piece.imgcolor[P1.i][P1.j].getG();
			int f2G = piece.imgcolor[P2.i][P2.j].getG();
			int f3G = piece.imgcolor[P3.i][P3.j].getG();
			int f4G = piece.imgcolor[P4.i][P4.j].getG();

			factor = (f1G+f2G+f3G+f4G)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1G*(x_floor - column)*(y_floor - row) + f3G*(column - x_ceiling)*(y_floor - row) + f2G*(x_floor-column)*(row - y_ceiling) + f4G*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			fixed_image.imgcolor[i][j].setG(factor);

			int f1B = piece.imgcolor[P1.i][P1.j].getB();
			int f2B = piece.imgcolor[P2.i][P2.j].getB();
			int f3B = piece.imgcolor[P3.i][P3.j].getB();
			int f4B = piece.imgcolor[P4.i][P4.j].getB();

			factor = (f1B+f2B+f3B+f4B)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1B*(x_floor - column)*(y_floor - row) + f3B*(column - x_ceiling)*(y_floor - row) + f2B*(x_floor-column)*(row - y_ceiling) + f4B*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			fixed_image.imgcolor[i][j].setB(factor);
		}
	}

	myfile.open("intermediate.raw", ios::out|ios::binary);
	for(int i = 0; i<120; i++){
		for(int j = 0; j< 120; j++){
			myfile.put(fixed_image.imgcolor[i][j].getR());
			myfile.put(fixed_image.imgcolor[i][j].getG());
			myfile.put(fixed_image.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//find the coordinates of the hole in the lion.raw pic
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 1\\lion.raw", ios::in|ios::binary);
	Image lion(400, 500, "color", myfile);
	myfile.close();

	myfile.open("lion_test.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j< 400; j++){
			myfile.put(lion.imgcolor[i][j].getR());
			myfile.put(lion.imgcolor[i][j].getG());
			myfile.put(lion.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	Point HolePoint1;
	for(int i = 0; i<500; i++){
		bool found = false;
		for(int j=0; j<400; j++){
			int count = 0;
			for(int k = 0; k<8; k++){
				for (int l= 0; l < 8; l++){
					if(lion.imgcolor[i+k][j+l].getB() == 255 && lion.imgcolor[i+k][j+l].getG() == 255 && lion.imgcolor[i+k][j+l].getR() == 255){
						count++;
					}
				}
			}
			//cout<<count<<endl;
			if(count == 64){
				//found the hole
				found = true;
				HolePoint1.i = i;
				HolePoint1.j = j;
				break;
			}
		}
		if(found == true){
			break;
		}
	}

	cout<<"the Lion hole first one found at point: "<<HolePoint1.j<<"  "<<HolePoint1.i<<endl;

	//from this point on wards we need to see how big the hole is.. I am assuming that we know that the hole is a rectangular figure
	Point HolePoint2;
	for(int i = HolePoint1.i; i<500; i++){
		if(lion.imgcolor[i][HolePoint1.j].getB() == 255 && lion.imgcolor[i][HolePoint1.j].getB() == 255 && lion.imgcolor[i][HolePoint1.j].getB() == 255){
			continue;
		}else{
			HolePoint2.i = i-1;
			HolePoint2.j = HolePoint1.j;
			break;
		}
	}

	Point HolePoint3;
	for(int j = HolePoint1.j; j<400; j++){
		if(lion.imgcolor[HolePoint1.i][j].getB() == 255 && lion.imgcolor[HolePoint1.i][j].getB() == 255 && lion.imgcolor[HolePoint1.i][j].getB() == 255){
			continue;
		}else{
			HolePoint3.i = HolePoint1.i;
			HolePoint3.j = j-1;
			break;
		}
	}

	Point HolePoint4(HolePoint2.i, HolePoint3.j);

	//The four points found to be:
	cout<<"The first Hole Point : "<<HolePoint1.j<<"  "<<HolePoint1.i<<endl;
	cout<<"The second Hole Point : "<<HolePoint2.j<<"  "<<HolePoint2.i<<endl;
	cout<<"The third Hodle Point : "<<HolePoint3.j<<"  "<<HolePoint3.i<<endl;
	cout<<"The fourth Hole Point : "<<HolePoint4.j<<"  "<<HolePoint4.i<<endl;

	//anyway we just needed the first hole point to fill the lion image in
	Image lion_puzzle_recontructed(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			lion_puzzle_recontructed.imgcolor[i][j].setB(lion.imgcolor[i][j].getB());
			lion_puzzle_recontructed.imgcolor[i][j].setG(lion.imgcolor[i][j].getG());
			lion_puzzle_recontructed.imgcolor[i][j].setR(lion.imgcolor[i][j].getR());
		}
	}
	
	for(int i=HolePoint1.i; i<HolePoint4.i+1; i++){
		for(int j=HolePoint1.j; j<HolePoint4.j+1; j++){
			lion_puzzle_recontructed.imgcolor[i][j].setB(fixed_image.imgcolor[i-HolePoint1.i][j-HolePoint1.j].getB());
			lion_puzzle_recontructed.imgcolor[i][j].setG(fixed_image.imgcolor[i-HolePoint1.i][j-HolePoint1.j].getG());
			lion_puzzle_recontructed.imgcolor[i][j].setR(fixed_image.imgcolor[i-HolePoint1.i][j-HolePoint1.j].getR());
		}
	}
	
	//median filtering required for the reconstructed image since we have a distinct white line in the reconstructed image
	for(int i=HolePoint1.i-3; i<HolePoint4.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint1.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getR();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setR(median_values[4]);
		}
	}

	for(int i=HolePoint1.i-3; i<HolePoint4.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint1.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getG();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setG(median_values[4]);
		}
	}

	for(int i=HolePoint1.i-3; i<HolePoint4.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint1.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getB();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setB(median_values[4]);
		}
	}

	for(int i=HolePoint1.i-3; i<HolePoint1.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint4.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getR();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getR();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setR(median_values[4]);
		}
	}

	for(int i=HolePoint1.i-3; i<HolePoint1.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint4.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getG();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getG();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setG(median_values[4]);
		}
	}

	for(int i=HolePoint1.i-3; i<HolePoint1.i+3; i++){
		for(int j=HolePoint1.j-3; j<HolePoint4.j+3; j++){
			int median_values[9];
			int count = 0;
			for(int m=-1; m<2; m++){
				for(int n=-1; n<2; n++){
					if(i+m > 499){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}else if(i+m < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}
					if(j+n > 399){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
						continue;
					}else if(j+n < 0){
						median_values[count++] = lion_puzzle_recontructed.imgcolor[i][j].getB();
					}

					median_values[count++] = lion_puzzle_recontructed.imgcolor[i+m][j+n].getB();
				}
			}
	
			//sort the median values
			qsort(median_values, 9, sizeof(int), compare);
			lion_puzzle_recontructed.imgcolor[i][j].setB(median_values[4]);
		}
	}
	//printing out the image for intermediate check
	myfile.open("lion_reconstructed.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(lion_puzzle_recontructed.imgcolor[i][j].getR());
			myfile.put(lion_puzzle_recontructed.imgcolor[i][j].getG());
			myfile.put(lion_puzzle_recontructed.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//Lion puzzle construction has been done.. lets move on to facial warping

	Image lion_face_warped(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			//we have to first decide which triange is the one we are working on
			float column, row;
			//int trinum = triangleNumber(j,i);
			if(triangleNumber(j,i) == 1){
				column = j*1+i*(-0.0199);
				row = j*0+i*(0.89054);
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 2){
				column = j*(0.9803)+i*(0);
				row = j*(-0.1084)+i*1;
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 3){
				column = j*1+i*(0.013378)-6.689;
				row = j*0+i*(1.0758)-36.7893;
			}else if(triangleNumber(j, i) == 4){
				column = j*(1.0203)+i*(0)-8.12;
				row = j*0.111675+i*(1)-44.67;
			}	
			
			if(column == 0 && row == 499){
				cout<<"doesn"<<endl;
			}
			//Now we do the bilinear interpolation
			int x_ceiling = (int)column + 1;
			int x_floor = (int)column;
			int y_ceiling = (int)row + 1;
			int y_floor = (int)row;

			//thresholding of points 
			if(x_ceiling < 0){
				x_ceiling = 0;
			}else if(x_ceiling > 399){
				x_ceiling =399;
			}

			if(x_floor < 0){
				x_floor = 0;
			}else if(x_floor > 399){
				x_floor =399;
			}

			if(y_ceiling < 0){
				y_ceiling = 0;
			}else if(y_ceiling > 499){
				y_ceiling =499;
			}

			if(y_floor < 0){
				y_floor = 0;
			}else if(y_floor > 499){
				y_floor =499;
			}
			//we can now create 4 points of interest
			Point P2(y_floor, x_ceiling);
			Point P1(y_ceiling, x_ceiling);
			Point P4(y_floor, x_floor);
			Point P3(y_ceiling, x_floor);

			//find the value at these points
			int f1R = lion_puzzle_recontructed.imgcolor[P1.i][P1.j].getR();
			int f2R = lion_puzzle_recontructed.imgcolor[P2.i][P2.j].getR();
			int f3R = lion_puzzle_recontructed.imgcolor[P3.i][P3.j].getR();
			int f4R = lion_puzzle_recontructed.imgcolor[P4.i][P4.j].getR();

			float factor = (f1R+f2R+f3R+f4R)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1R*(x_floor - column)*(y_floor - row) + f3R*(column - x_ceiling)*(y_floor - row) + f2R*(x_floor-column)*(row - y_ceiling) + f4R*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			lion_face_warped.imgcolor[i][j].setR(factor);

			int f1G = lion_puzzle_recontructed.imgcolor[P1.i][P1.j].getG();
			int f2G = lion_puzzle_recontructed.imgcolor[P2.i][P2.j].getG();
			int f3G = lion_puzzle_recontructed.imgcolor[P3.i][P3.j].getG();
			int f4G = lion_puzzle_recontructed.imgcolor[P4.i][P4.j].getG();

			factor = (f1G+f2G+f3G+f4G)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1G*(x_floor - column)*(y_floor - row) + f3G*(column - x_ceiling)*(y_floor - row) + f2G*(x_floor-column)*(row - y_ceiling) + f4G*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			lion_face_warped.imgcolor[i][j].setG(factor);

			int f1B = lion_puzzle_recontructed.imgcolor[P1.i][P1.j].getB();
			int f2B = lion_puzzle_recontructed.imgcolor[P2.i][P2.j].getB();
			int f3B = lion_puzzle_recontructed.imgcolor[P3.i][P3.j].getB();
			int f4B = lion_puzzle_recontructed.imgcolor[P4.i][P4.j].getB();

			factor = (f1B+f2B+f3B+f4B)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1B*(x_floor - column)*(y_floor - row) + f3B*(column - x_ceiling)*(y_floor - row) + f2B*(x_floor-column)*(row - y_ceiling) + f4B*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			lion_face_warped.imgcolor[i][j].setB(factor);
		}
	}

	myfile.open("lion_face_warped.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(lion_face_warped.imgcolor[i][j].getR());
			myfile.put(lion_face_warped.imgcolor[i][j].getG());
			myfile.put(lion_face_warped.imgcolor[i][j].getB());
		}
	}
	myfile.close();


	//now the lion warping done, we will begin the warping for romney
	string filename2;
	cout<<"Enter a filename: "<<filename2;
	cin>>filename2;
	myfile.open(filename2, ios::in|ios::binary);
	Image romney(400, 500, "color", myfile);
	myfile.close();

	myfile.open("romney_test.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney.imgcolor[i][j].getR());
			myfile.put(romney.imgcolor[i][j].getG());
			myfile.put(romney.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	Image romney_wrap(400, 500, "color");
	
	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			//we have to first decide which triange is the one we are working on
			float column, row;
			//int trinum = triangleNumber(j,i);
			if(triangleNumber(j,i) == 1){
				column = j*1+i*(0.01493);
				row = j*0+i*(1.10945);
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 2){
				column = j*(1.014778)+i*(0);
				row = j*(0.108374)+i*1;
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 3){
				column = j*1+i*(-0.01003)+5.0167224;
				row = j*0+i*(0.92642)+36.78930;
			}else if(triangleNumber(j, i) == 4){
				column = j*(0.98477)+i*(0)+6.092;
				row = j*(-0.111675)+i*(1)+44.67005;
			}	
			
			if(column == 0 && row == 499){
				cout<<"doesn"<<endl;
			}
			//Now we do the bilinear interpolation
			int x_ceiling = (int)column + 1;
			int x_floor = (int)column;
			int y_ceiling = (int)row + 1;
			int y_floor = (int)row;

			//thresholding of points 
			if(x_ceiling < 0){
				x_ceiling = 0;
			}else if(x_ceiling > 399){
				x_ceiling =399;
			}

			if(x_floor < 0){
				x_floor = 0;
			}else if(x_floor > 399){
				x_floor =399;
			}

			if(y_ceiling < 0){
				y_ceiling = 0;
			}else if(y_ceiling > 499){
				y_ceiling =499;
			}

			if(y_floor < 0){
				y_floor = 0;
			}else if(y_floor > 499){
				y_floor =499;
			}
			//we can now create 4 points of interest
			Point P2(y_floor, x_ceiling);
			Point P1(y_ceiling, x_ceiling);
			Point P4(y_floor, x_floor);
			Point P3(y_ceiling, x_floor);

			//find the value at these points
			int f1R = romney.imgcolor[P1.i][P1.j].getR();
			int f2R = romney.imgcolor[P2.i][P2.j].getR();
			int f3R = romney.imgcolor[P3.i][P3.j].getR();
			int f4R = romney.imgcolor[P4.i][P4.j].getR();

			float factor = (f1R+f2R+f3R+f4R)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1R*(x_floor - column)*(y_floor - row) + f3R*(column - x_ceiling)*(y_floor - row) + f2R*(x_floor-column)*(row - y_ceiling) + f4R*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setR(factor);

			int f1G = romney.imgcolor[P1.i][P1.j].getG();
			int f2G = romney.imgcolor[P2.i][P2.j].getG();
			int f3G = romney.imgcolor[P3.i][P3.j].getG();
			int f4G = romney.imgcolor[P4.i][P4.j].getG();

			factor = (f1G+f2G+f3G+f4G)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1G*(x_floor - column)*(y_floor - row) + f3G*(column - x_ceiling)*(y_floor - row) + f2G*(x_floor-column)*(row - y_ceiling) + f4G*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setG(factor);

			int f1B = romney.imgcolor[P1.i][P1.j].getB();
			int f2B = romney.imgcolor[P2.i][P2.j].getB();
			int f3B = romney.imgcolor[P3.i][P3.j].getB();
			int f4B = romney.imgcolor[P4.i][P4.j].getB();

			factor = (f1B+f2B+f3B+f4B)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1B*(x_floor - column)*(y_floor - row) + f3B*(column - x_ceiling)*(y_floor - row) + f2B*(x_floor-column)*(row - y_ceiling) + f4B*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setB(factor);
		}
	}

	myfile.open("romney_face_warped.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_wrap.imgcolor[i][j].getR());
			myfile.put(romney_wrap.imgcolor[i][j].getG());
			myfile.put(romney_wrap.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.2
	Image romney_tiger_alpha_2(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_tiger_alpha_2.imgcolor[i][j].setB(0.8*lion_face_warped.imgcolor[i][j].getB() + 0.2*romney_wrap.imgcolor[i][j].getB());
			romney_tiger_alpha_2.imgcolor[i][j].setG(0.8*lion_face_warped.imgcolor[i][j].getG() + 0.2*romney_wrap.imgcolor[i][j].getG());
			romney_tiger_alpha_2.imgcolor[i][j].setR(0.8*lion_face_warped.imgcolor[i][j].getR() + 0.2*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_tiger_alpha_2.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_tiger_alpha_2.imgcolor[i][j].getR());
			myfile.put(romney_tiger_alpha_2.imgcolor[i][j].getG());
			myfile.put(romney_tiger_alpha_2.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.5
	Image romney_tiger_alpha_5(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_tiger_alpha_5.imgcolor[i][j].setB(0.5*lion_face_warped.imgcolor[i][j].getB() + 0.5*romney_wrap.imgcolor[i][j].getB());
			romney_tiger_alpha_5.imgcolor[i][j].setG(0.5*lion_face_warped.imgcolor[i][j].getG() + 0.5*romney_wrap.imgcolor[i][j].getG());
			romney_tiger_alpha_5.imgcolor[i][j].setR(0.5*lion_face_warped.imgcolor[i][j].getR() + 0.5*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_tiger_alpha_5.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_tiger_alpha_5.imgcolor[i][j].getR());
			myfile.put(romney_tiger_alpha_5.imgcolor[i][j].getG());
			myfile.put(romney_tiger_alpha_5.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.8
	Image romney_tiger_alpha_8(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_tiger_alpha_8.imgcolor[i][j].setB(0.2*lion_face_warped.imgcolor[i][j].getB() + 0.8*romney_wrap.imgcolor[i][j].getB());
			romney_tiger_alpha_8.imgcolor[i][j].setG(0.2*lion_face_warped.imgcolor[i][j].getG() + 0.8*romney_wrap.imgcolor[i][j].getG());
			romney_tiger_alpha_8.imgcolor[i][j].setR(0.2*lion_face_warped.imgcolor[i][j].getR() + 0.8*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_tiger_alpha_8.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_tiger_alpha_8.imgcolor[i][j].getR());
			myfile.put(romney_tiger_alpha_8.imgcolor[i][j].getG());
			myfile.put(romney_tiger_alpha_8.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//Obama face warping
	string filename1;
	cout<<"Enter first fileName: " <<filename1;
	cin>>filename1;
	myfile.open(filename1, ios::in|ios::binary);
	Image obama(400, 500, "color", myfile);
	myfile.close();

	Image obama_warp(400, 500, "color");
	
	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			//we have to first decide which triange is the one we are working on
			float column, row;
			//int trinum = triangleNumber(j,i);
			if(triangleNumber(j,i) == 1){
				column = j*1+i*(0.046729);
				row = j*0+i*(0.957943);
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 2){
				column = j*(1.04717)+i*(0);
				row = j*(-0.04245)+i*1;
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 3){
				column = j*1+i*(-0.034965)+17.48251;
				row = j*0+i*(1.031469)-15.734366;
			}else if(triangleNumber(j, i) == 4){
				column = j*(0.94680)+i*(0)+21.276596;
				row = j*(0.047872)+i*(1)-19.148936;
			}	
			
			if(column == 0 && row == 499){
				cout<<"doesn"<<endl;
			}
			//Now we do the bilinear interpolation
			int x_ceiling = (int)column + 1;
			int x_floor = (int)column;
			int y_ceiling = (int)row + 1;
			int y_floor = (int)row;

			//thresholding of points 
			if(x_ceiling < 0){
				x_ceiling = 0;
			}else if(x_ceiling > 399){
				x_ceiling =399;
			}

			if(x_floor < 0){
				x_floor = 0;
			}else if(x_floor > 399){
				x_floor =399;
			}

			if(y_ceiling < 0){
				y_ceiling = 0;
			}else if(y_ceiling > 499){
				y_ceiling =499;
			}

			if(y_floor < 0){
				y_floor = 0;
			}else if(y_floor > 499){
				y_floor =499;
			}
			//we can now create 4 points of interest
			Point P2(y_floor, x_ceiling);
			Point P1(y_ceiling, x_ceiling);
			Point P4(y_floor, x_floor);
			Point P3(y_ceiling, x_floor);

			//find the value at these points
			int f1R = obama.imgcolor[P1.i][P1.j].getR();
			int f2R = obama.imgcolor[P2.i][P2.j].getR();
			int f3R = obama.imgcolor[P3.i][P3.j].getR();
			int f4R = obama.imgcolor[P4.i][P4.j].getR();

			float factor = (f1R+f2R+f3R+f4R)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1R*(x_floor - column)*(y_floor - row) + f3R*(column - x_ceiling)*(y_floor - row) + f2R*(x_floor-column)*(row - y_ceiling) + f4R*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			obama_warp.imgcolor[i][j].setR(factor);

			int f1G = obama.imgcolor[P1.i][P1.j].getG();
			int f2G = obama.imgcolor[P2.i][P2.j].getG();
			int f3G = obama.imgcolor[P3.i][P3.j].getG();
			int f4G = obama.imgcolor[P4.i][P4.j].getG();

			factor = (f1G+f2G+f3G+f4G)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1G*(x_floor - column)*(y_floor - row) + f3G*(column - x_ceiling)*(y_floor - row) + f2G*(x_floor-column)*(row - y_ceiling) + f4G*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			obama_warp.imgcolor[i][j].setG(factor);

			int f1B = obama.imgcolor[P1.i][P1.j].getB();
			int f2B = obama.imgcolor[P2.i][P2.j].getB();
			int f3B = obama.imgcolor[P3.i][P3.j].getB();
			int f4B = obama.imgcolor[P4.i][P4.j].getB();

			factor = (f1B+f2B+f3B+f4B)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1B*(x_floor - column)*(y_floor - row) + f3B*(column - x_ceiling)*(y_floor - row) + f2B*(x_floor-column)*(row - y_ceiling) + f4B*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			obama_warp.imgcolor[i][j].setB(factor);
		}
	}

	myfile.open("obama_face_warped.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(obama_warp.imgcolor[i][j].getR());
			myfile.put(obama_warp.imgcolor[i][j].getG());
			myfile.put(obama_warp.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//Romney for Obama
	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			//we have to first decide which triange is the one we are working on
			float column, row;
			//int trinum = triangleNumber(j,i);
			if(triangleNumber(j,i) == 1){
				column = j*1+i*(-0.042056);
				row = j*0+i*(1.042056);
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 2){
				column = j*(0.957547)+i*(0);
				row = j*(0.04245)+i*1;
				//cout<<"The coordinates of the points that pass this test: "<<column<<" "<<row<<endl;
			}else if(triangleNumber(j, i) == 3){
				column = j*1+i*(0.0314685)-15.734266;
				row = j*0+i*(0.968531)+15.7342657;
			}else if(triangleNumber(j, i) == 4){
				column = j*(1.0478723)+i*(0)-19.148936;
				row = j*(-0.0478723)+i*(1)+19.14893;
			}	
			
			if(column == 0 && row == 499){
				cout<<"doesn"<<endl;
			}
			//Now we do the bilinear interpolation
			int x_ceiling = (int)column + 1;
			int x_floor = (int)column;
			int y_ceiling = (int)row + 1;
			int y_floor = (int)row;

			//thresholding of points 
			if(x_ceiling < 0){
				x_ceiling = 0;
			}else if(x_ceiling > 399){
				x_ceiling =399;
			}

			if(x_floor < 0){
				x_floor = 0;
			}else if(x_floor > 399){
				x_floor =399;
			}

			if(y_ceiling < 0){
				y_ceiling = 0;
			}else if(y_ceiling > 499){
				y_ceiling =499;
			}

			if(y_floor < 0){
				y_floor = 0;
			}else if(y_floor > 499){
				y_floor =499;
			}
			//we can now create 4 points of interest
			Point P2(y_floor, x_ceiling);
			Point P1(y_ceiling, x_ceiling);
			Point P4(y_floor, x_floor);
			Point P3(y_ceiling, x_floor);

			//find the value at these points
			int f1R = romney.imgcolor[P1.i][P1.j].getR();
			int f2R = romney.imgcolor[P2.i][P2.j].getR();
			int f3R = romney.imgcolor[P3.i][P3.j].getR();
			int f4R = romney.imgcolor[P4.i][P4.j].getR();

			float factor = (f1R+f2R+f3R+f4R)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1R*(x_floor - column)*(y_floor - row) + f3R*(column - x_ceiling)*(y_floor - row) + f2R*(x_floor-column)*(row - y_ceiling) + f4R*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setR(factor);

			int f1G = romney.imgcolor[P1.i][P1.j].getG();
			int f2G = romney.imgcolor[P2.i][P2.j].getG();
			int f3G = romney.imgcolor[P3.i][P3.j].getG();
			int f4G = romney.imgcolor[P4.i][P4.j].getG();

			factor = (f1G+f2G+f3G+f4G)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1G*(x_floor - column)*(y_floor - row) + f3G*(column - x_ceiling)*(y_floor - row) + f2G*(x_floor-column)*(row - y_ceiling) + f4G*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setG(factor);

			int f1B = romney.imgcolor[P1.i][P1.j].getB();
			int f2B = romney.imgcolor[P2.i][P2.j].getB();
			int f3B = romney.imgcolor[P3.i][P3.j].getB();
			int f4B = romney.imgcolor[P4.i][P4.j].getB();

			factor = (f1B+f2B+f3B+f4B)/4;
			if(x_ceiling == x_floor || y_ceiling == y_floor){
				//dont do the factor thing
			}else{
				factor = f1B*(x_floor - column)*(y_floor - row) + f3B*(column - x_ceiling)*(y_floor - row) + f2B*(x_floor-column)*(row - y_ceiling) + f4B*(column - x_ceiling)*(row - y_ceiling);
				factor = (1/((x_floor - x_ceiling)*(y_floor - y_ceiling)))*factor;
			}

			if(factor > 255){
				factor = 255;
			}else if(factor < 0){
				factor = 0;
			}

			romney_wrap.imgcolor[i][j].setB(factor);
		}
	}

	myfile.open("romneyobama_face_warped.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_wrap.imgcolor[i][j].getR());
			myfile.put(romney_wrap.imgcolor[i][j].getG());
			myfile.put(romney_wrap.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.2
	Image romney_obama_2(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_obama_2.imgcolor[i][j].setB(0.1*obama_warp.imgcolor[i][j].getB() + 0.9*romney_wrap.imgcolor[i][j].getB());
			romney_obama_2.imgcolor[i][j].setG(0.1*obama_warp.imgcolor[i][j].getG() + 0.9*romney_wrap.imgcolor[i][j].getG());
			romney_obama_2.imgcolor[i][j].setR(0.1*obama_warp.imgcolor[i][j].getR() + 0.9*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_obama_alpha_2.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_obama_2.imgcolor[i][j].getR());
			myfile.put(romney_obama_2.imgcolor[i][j].getG());
			myfile.put(romney_obama_2.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.5
	Image romney_obama_alpha_5(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_obama_alpha_5.imgcolor[i][j].setB(0.3*obama_warp.imgcolor[i][j].getB() + 0.7*romney_wrap.imgcolor[i][j].getB());
			romney_obama_alpha_5.imgcolor[i][j].setG(0.3*obama_warp.imgcolor[i][j].getG() + 0.7*romney_wrap.imgcolor[i][j].getG());
			romney_obama_alpha_5.imgcolor[i][j].setR(0.3*obama_warp.imgcolor[i][j].getR() + 0.7*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_obama_alpha_5.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_obama_alpha_5.imgcolor[i][j].getR());
			myfile.put(romney_obama_alpha_5.imgcolor[i][j].getG());
			myfile.put(romney_obama_alpha_5.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//alpha = 0.8
	Image romney_obama_alpha_8(400, 500, "color");

	for(int i=0; i<500; i++){
		for(int j=0; j<400; j++){
			romney_obama_alpha_8.imgcolor[i][j].setB(0.4*obama_warp.imgcolor[i][j].getB() + 0.6*romney_wrap.imgcolor[i][j].getB());
			romney_obama_alpha_8.imgcolor[i][j].setG(0.4*obama_warp.imgcolor[i][j].getG() + 0.6*romney_wrap.imgcolor[i][j].getG());
			romney_obama_alpha_8.imgcolor[i][j].setR(0.4*obama_warp.imgcolor[i][j].getR() + 0.6*romney_wrap.imgcolor[i][j].getR());
		}
	}

	myfile.open("romney_obama_alpha_8.raw", ios::out|ios::binary);
	for(int i = 0; i<500; i++){
		for(int j = 0; j<400; j++){
			myfile.put(romney_obama_alpha_8.imgcolor[i][j].getR());
			myfile.put(romney_obama_alpha_8.imgcolor[i][j].getG());
			myfile.put(romney_obama_alpha_8.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	Image romney_obama_alpha(400, 500, "color");

	int count = 1;
	ostringstream cc;
	for(float alpha = 0.1; alpha < 1; alpha = alpha+0.02){
		cout<<alpha<<endl;
		for(int i=0; i<500; i++){
			for(int j=0; j<400; j++){
				romney_obama_alpha.imgcolor[i][j].setB(alpha*obama_warp.imgcolor[i][j].getB() + (1-alpha)*romney_wrap.imgcolor[i][j].getB());
				romney_obama_alpha.imgcolor[i][j].setG(alpha*obama_warp.imgcolor[i][j].getG() + (1-alpha)*romney_wrap.imgcolor[i][j].getG());
				romney_obama_alpha.imgcolor[i][j].setR(alpha*obama_warp.imgcolor[i][j].getR() + (1-alpha)*romney_wrap.imgcolor[i][j].getR());
			}
		}
		string filename = "this";
		
		cc<<count;
		filename.append(cc.str());
		myfile.open(filename, ios::out|ios::binary);
		for(int i = 0; i<500; i++){
			for(int j = 0; j<400; j++){
				myfile.put(romney_obama_alpha_8.imgcolor[i][j].getR());
				myfile.put(romney_obama_alpha_8.imgcolor[i][j].getG());
				myfile.put(romney_obama_alpha_8.imgcolor[i][j].getB());
			}
		}
		myfile.close();
		count++;
	}

	/********************************************end of problem 1******************************************************************************/


	/******************************************start of problem 2********************************************************************************/
	/*
	vector< vector<int> > L5(5, vector<int>(1));
	vector< vector<int> > E5(5, vector<int>(1));
	vector< vector<int> > S5(5, vector<int>(1));
	vector< vector<int> > W5(5, vector<int>(1));
	vector< vector<int> > R5(5, vector<int>(1));

	L5[0][0] = 1; L5[1][0] = 4; L5[2][0] = 6; L5[3][0] = 4; L5[4][0] = 1;                          //Level
	E5[0][0] = -1; E5[1][0] = -2; E5[2][0] = 0; E5[3][0] = 2; E5[4][0] = 1;                      //Edge
	S5[0][0] = -1; S5[1][0] = 0; S5[2][0] = 2; S5[3][0] = 0; S5[4][0] = -1;                      //Spot
	W5[0][0] = -1; W5[1][0] = 2; W5[2][0] = 0; W5[3][0] = -2; W5[4][0] = 1;                 //Wave
	R5[0][0] = 1; R5[1][0] = -4; R5[2][0] = 6; R5[3][0] = -4; R5[4][0] = 1;                  //Ripple

	vector< vector<int> > L5L5 = matrix_multiply(L5, L5);
	vector< vector<int> > L5E5 = matrix_multiply(L5, E5);
	vector< vector<int> > L5S5 = matrix_multiply(L5, S5);
	vector< vector<int> > L5W5 = matrix_multiply(L5, W5);
	vector< vector<int> > L5R5 = matrix_multiply(L5, R5);

	vector< vector<int> > E5L5 = matrix_multiply(E5, L5);
	vector< vector<int> > E5E5 = matrix_multiply(E5, E5);
	vector< vector<int> > E5S5 = matrix_multiply(E5, S5);
	vector< vector<int> > E5W5 = matrix_multiply(E5, W5);
	vector< vector<int> > E5R5 = matrix_multiply(E5, R5);

	vector< vector<int> > S5L5 = matrix_multiply(S5, L5);
	vector< vector<int> > S5E5 = matrix_multiply(S5, E5);
	vector< vector<int> > S5S5 = matrix_multiply(S5, S5);
	vector< vector<int> > S5W5 = matrix_multiply(S5, W5);
	vector< vector<int> > S5R5 = matrix_multiply(S5, R5);

	vector< vector<int> > W5L5 = matrix_multiply(W5, L5);
	vector< vector<int> > W5E5 = matrix_multiply(W5, E5);
	vector< vector<int> > W5S5 = matrix_multiply(W5, S5);
	vector< vector<int> > W5W5 = matrix_multiply(W5, W5);
	vector< vector<int> > W5R5 = matrix_multiply(W5, R5);

	vector< vector<int> > R5L5 = matrix_multiply(R5, L5);
	vector< vector<int> > R5E5 = matrix_multiply(R5, E5);
	vector< vector<int> > R5S5 = matrix_multiply(R5, S5);
	vector< vector<int> > R5W5 = matrix_multiply(R5, W5);
	vector< vector<int> > R5R5 = matrix_multiply(R5, R5);

	myfile.open("matrices.txt", ios::out);
	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<L5L5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<L5E5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<L5S5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<L5W5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<L5R5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<E5L5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<E5E5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<E5S5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<E5W5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<E5R5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<S5L5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<S5E5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<S5S5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<S5W5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<S5R5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<W5L5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<W5E5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<W5S5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<W5W5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<W5R5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<R5L5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<R5E5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<R5S5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<R5W5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile<<endl<<endl<<endl;

	for(int i=0; i<L5L5.size(); i++){
		for(int j=0; j<L5L5.size(); j++){
			myfile<<R5R5[i][j]<<" ";
		}
		myfile<<endl;
	}
	myfile.close();

	

	//We have to filter the image with all of these filters now.. 
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 2\\comb.raw", ios::in|ios::binary);
	Image comb(256, 256, "gray", myfile);
	myfile.close();
	
	myfile.open("comb_test.raw", ios::out|ios::binary);
	for(int i = 0; i<256; i++){
		for(int j = 0; j<256; j++){
			myfile.put(comb.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	Image comb_L5L5 = filtered_image(comb, L5L5);
	Image comb_L5E5 = filtered_image(comb, L5E5);
	Image comb_L5S5 = filtered_image(comb, L5S5);
	Image comb_L5W5 = filtered_image(comb, L5W5);
	Image comb_L5R5 = filtered_image(comb, L5R5);

	Image comb_E5L5 = filtered_image(comb, E5L5);
	Image comb_E5E5 = filtered_image(comb, E5E5);
	Image comb_E5S5 = filtered_image(comb, E5S5);
	Image comb_E5W5 = filtered_image(comb, E5W5);
	Image comb_E5R5 = filtered_image(comb, E5R5);

	Image comb_S5L5 = filtered_image(comb, S5L5);
	Image comb_S5E5 = filtered_image(comb, S5E5);
	Image comb_S5S5 = filtered_image(comb, S5S5);
	Image comb_S5W5 = filtered_image(comb, S5W5);
	Image comb_S5R5 = filtered_image(comb, S5R5);

	Image comb_W5L5 = filtered_image(comb, W5L5);
	Image comb_W5E5 = filtered_image(comb, W5E5);
	Image comb_W5S5 = filtered_image(comb, W5S5);
	Image comb_W5W5 = filtered_image(comb, W5W5);
	Image comb_W5R5 = filtered_image(comb, W5R5);

	Image comb_R5L5 = filtered_image(comb, R5L5);
	Image comb_R5E5 = filtered_image(comb, R5E5);
	Image comb_R5S5 = filtered_image(comb, R5S5);
	Image comb_R5W5 = filtered_image(comb, R5W5);
	Image comb_R5R5 = filtered_image(comb, R5R5);

	//now we have to find the TEM images.. using windowing
	float **comb_L5L5_T;
	comb_L5L5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_L5L5_T[i] = new float[256];
	}
	float **comb_L5E5_T;
	comb_L5E5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_L5E5_T[i] = new float[256];
	}
	float **comb_L5S5_T;
	comb_L5S5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_L5S5_T[i] = new float[256];
	}
	float **comb_L5W5_T;
	comb_L5W5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_L5W5_T[i] = new float[256];
	}
	float **comb_L5R5_T;
	comb_L5R5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_L5R5_T[i] = new float[256];
	}

	float **comb_E5L5_T;
	comb_E5L5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_E5L5_T[i] = new float[256];
	}
	float **comb_E5E5_T;
	comb_E5E5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_E5E5_T[i] = new float[256];
	}
	float **comb_E5S5_T;
	comb_E5S5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_E5S5_T[i] = new float[256];
	}
	float **comb_E5W5_T;
	comb_E5W5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_E5W5_T[i] = new float[256];
	}
	float **comb_E5R5_T;
	comb_E5R5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_E5R5_T[i] = new float[256];
	}

	float **comb_S5L5_T;
	comb_S5L5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_S5L5_T[i] = new float[256];
	}
	float **comb_S5E5_T;
	comb_S5E5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_S5E5_T[i] = new float[256];
	}
	float **comb_S5S5_T;
	comb_S5S5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_S5S5_T[i] = new float[256];
	}
	float **comb_S5W5_T;
	comb_S5W5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_S5W5_T[i] = new float[256];
	}
	float **comb_S5R5_T;
	comb_S5R5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_S5R5_T[i] = new float[256];
	}

	float **comb_W5L5_T;
	comb_W5L5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_W5L5_T[i] = new float[256];
	}
	float **comb_W5E5_T;
	comb_W5E5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_W5E5_T[i] = new float[256];
	}
	float **comb_W5S5_T;
	comb_W5S5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_W5S5_T[i] = new float[256];
	}
	float **comb_W5W5_T;
	comb_W5W5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_W5W5_T[i] = new float[256];
	}
	float **comb_W5R5_T;
	comb_W5R5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_W5R5_T[i] = new float[256];
	}

	float **comb_R5L5_T;
	comb_R5L5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_R5L5_T[i] = new float[256];
	}
	float **comb_R5E5_T;
	comb_R5E5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_R5E5_T[i] = new float[256];
	}
	float **comb_R5S5_T;
	comb_R5S5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_R5S5_T[i] = new float[256];
	}
	float **comb_R5W5_T;
	comb_R5W5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_R5W5_T[i] = new float[256];
	}
	float **comb_R5R5_T;
	comb_R5R5_T = new float*[256];
	for(int i=0; i<256; i++){
		comb_R5R5_T[i] = new float[256];
	}

	windowing(comb_L5L5, comb_L5L5_T);
	windowing(comb_L5E5, comb_L5E5_T);
	windowing(comb_L5S5, comb_L5S5_T);
	windowing(comb_L5W5, comb_L5W5_T);
	windowing(comb_L5R5, comb_L5R5_T);

	windowing(comb_E5L5, comb_E5L5_T);
	windowing(comb_E5E5, comb_E5E5_T);
	windowing(comb_E5S5, comb_E5S5_T);
	windowing(comb_E5W5, comb_E5W5_T);
	windowing(comb_E5R5, comb_E5R5_T);

	windowing(comb_S5L5, comb_S5L5_T);
	windowing(comb_S5E5, comb_S5E5_T);
	windowing(comb_S5S5, comb_S5S5_T);
	windowing(comb_S5W5, comb_S5W5_T);
	windowing(comb_S5R5, comb_S5R5_T);
	
	windowing(comb_W5L5, comb_W5L5_T);
	windowing(comb_W5E5, comb_W5E5_T);
	windowing(comb_W5S5, comb_W5S5_T);
	windowing(comb_W5W5, comb_W5W5_T);
	windowing(comb_W5R5, comb_W5R5_T);

	windowing(comb_R5L5, comb_R5L5_T);
	windowing(comb_R5E5, comb_R5E5_T);
	windowing(comb_R5S5, comb_R5S5_T);
	windowing(comb_R5W5, comb_R5W5_T);
	windowing(comb_R5R5, comb_R5R5_T);
	
	//Normalize the 24 maps with the first L5L5_T map

	//lets print out one of the maps and see what values we get

	normalize(comb_L5L5_T, comb_L5L5_T);
	normalize(comb_L5E5_T, comb_L5L5_T);
	normalize(comb_L5S5_T, comb_L5L5_T);
	normalize(comb_L5W5_T, comb_L5L5_T);
	normalize(comb_L5R5_T, comb_L5L5_T);

	normalize(comb_E5L5_T, comb_L5L5_T);
	normalize(comb_E5E5_T, comb_L5L5_T);
	normalize(comb_E5S5_T, comb_L5L5_T);
	normalize(comb_E5W5_T, comb_L5L5_T);
	normalize(comb_E5R5_T, comb_L5L5_T);

	normalize(comb_S5L5_T, comb_L5L5_T);
	normalize(comb_S5E5_T, comb_L5L5_T);
	normalize(comb_S5S5_T, comb_L5L5_T);
	normalize(comb_S5W5_T, comb_L5L5_T);
	normalize(comb_S5R5_T, comb_L5L5_T);

	normalize(comb_W5L5_T, comb_L5L5_T);
	normalize(comb_W5E5_T, comb_L5L5_T);
	normalize(comb_W5S5_T, comb_L5L5_T);
	normalize(comb_W5W5_T, comb_L5L5_T);
	normalize(comb_W5R5_T, comb_L5L5_T);

	normalize(comb_R5L5_T, comb_L5L5_T);
	normalize(comb_R5E5_T, comb_L5L5_T);
	normalize(comb_R5S5_T, comb_L5L5_T);
	normalize(comb_R5W5_T, comb_L5L5_T);
	normalize(comb_R5R5_T, comb_L5L5_T);
	
	//lets begin the K-means algorithm now
	//before we begin the algorithm it will be easier if we have only 1 data structure holding all the TEM images
	float*** TEM_COMB;
	TEM_COMB = new float**[256];
	for(int i=0; i<256; i++){
		TEM_COMB[i] = new float*[256];
	}
	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			TEM_COMB[i][j] = new float[24];
		}
	}

	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			TEM_COMB[i][j][0] = comb_L5E5_T[i][j];
			TEM_COMB[i][j][1] = comb_L5S5_T[i][j];
			TEM_COMB[i][j][2] = comb_L5W5_T[i][j];
			TEM_COMB[i][j][3] = comb_L5R5_T[i][j];

			TEM_COMB[i][j][4] = comb_E5L5_T[i][j];
			TEM_COMB[i][j][5] = comb_E5E5_T[i][j];
			TEM_COMB[i][j][6] = comb_E5S5_T[i][j];
			TEM_COMB[i][j][7] = comb_E5W5_T[i][j];
			TEM_COMB[i][j][8] = comb_E5R5_T[i][j];
			
			TEM_COMB[i][j][9] = comb_S5L5_T[i][j];
			TEM_COMB[i][j][10] = comb_S5E5_T[i][j];
			TEM_COMB[i][j][11] = comb_S5S5_T[i][j];
			TEM_COMB[i][j][12] = comb_S5W5_T[i][j];
			TEM_COMB[i][j][13] = comb_S5R5_T[i][j];

			TEM_COMB[i][j][14] = comb_W5L5_T[i][j];
			TEM_COMB[i][j][15] = comb_W5E5_T[i][j];
			TEM_COMB[i][j][16] = comb_W5S5_T[i][j];
			TEM_COMB[i][j][17] = comb_W5W5_T[i][j];
			TEM_COMB[i][j][18] = comb_W5R5_T[i][j];

			TEM_COMB[i][j][19] = comb_R5L5_T[i][j];
			TEM_COMB[i][j][20] = comb_R5E5_T[i][j];
			TEM_COMB[i][j][21] = comb_R5S5_T[i][j];
			TEM_COMB[i][j][22] = comb_R5W5_T[i][j];
			TEM_COMB[i][j][23] = comb_R5R5_T[i][j];
		}
	}

	Point mid_point1(20, 20);
	Point mid_point2(255, 20);
	Point mid_point3(127, 127);
	Point mid_point4(20, 255);
	Point mid_point5(235, 235);

	vector<Point*> cluster1;
	vector<Point*> cluster2;
	vector<Point*> cluster3;
	vector<Point*> cluster4;
	vector<Point*> cluster5;

	bool finish_looping = false;
	int count = 0;
	while(!finish_looping){
		for(int i=0; i<256; i++){
			for(int j=0; j<256; j++){
				//find the distance of each point with the mid_point
				float distance[5];
				float sum = 0;
				for(int k=0; k<24; k++){
					sum += pow(TEM_COMB[i][j][k] - TEM_COMB[mid_point1.i][mid_point1.j][k],2); 
				}
				sum = sqrtf(sum);
				distance[0] = sum;
				sum = 0;
				for(int k=0; k<24; k++){
					sum += pow(TEM_COMB[i][j][k] - TEM_COMB[mid_point2.i][mid_point2.j][k],2); 
				}
				sum = sqrtf(sum);
				distance[1] = sum;
				sum = 0;
				for(int k=0; k<24; k++){
					sum += pow(TEM_COMB[i][j][k] - TEM_COMB[mid_point3.i][mid_point3.j][k],2); 
				}
				sum = sqrtf(sum);
				distance[2] = sum;

				sum = 0;
				for(int k=0; k<24; k++){
					sum += pow(TEM_COMB[i][j][k] - TEM_COMB[mid_point4.i][mid_point4.j][k],2); 
				}
				sum = sqrtf(sum);
				distance[3] = sum;
				sum = 0;
				for(int k=0; k<24; k++){
					sum += pow(TEM_COMB[i][j][k] - TEM_COMB[mid_point5.i][mid_point5.j][k],2); 
				}
				sum = sqrtf(sum);
				distance[4] = sum;

				float minimum = distance[0];
				int min_index = 0;
				for(int k=0; k<5; k++){
					if(minimum > distance[k]){
						minimum = distance[k];
						min_index = k;
					}
				}

				if(min_index == 0){
					cluster1.push_back(new Point(i,j));
				}else if(min_index == 1){
					cluster2.push_back(new Point(i,j));
				}else if(min_index == 2){
					cluster3.push_back(new Point(i,j));
				}else if(min_index == 3){
					cluster4.push_back(new Point(i,j));
				}else if(min_index == 4){
					cluster5.push_back(new Point(i,j));
				}
			}
		}
		//find the new mid_points for the clusters
		float median_sum_i = 0, median_sum_j = 0;
		int count_med=0;
		for(int i = 0; i<cluster1.size(); i++){
			median_sum_i += cluster1[i]->i;
			median_sum_j += cluster1[i]->j;
			count_med++;
		}

		Point new_med_point1(median_sum_i/count_med, median_sum_j/count_med);
	
		median_sum_i = 0; median_sum_j = 0;
		count_med = 0;
		for(int i = 0; i<cluster2.size(); i++){
			median_sum_i += cluster2[i]->i;
			median_sum_j += cluster2[i]->j;
			count_med++;
		}

		Point new_med_point2(median_sum_i/count_med, median_sum_j/count_med);

		median_sum_i = 0; median_sum_j = 0;
		count_med = 0;
		for(int i = 0; i<cluster3.size(); i++){
			median_sum_i += cluster3[i]->i;
			median_sum_j += cluster3[i]->j;
			count_med++;
		}

		Point new_med_point3(median_sum_i/count_med, median_sum_j/count_med);

		median_sum_i = 0; median_sum_j = 0;
		count_med = 0;
		for(int i = 0; i<cluster4.size(); i++){
			median_sum_i += cluster4[i]->i;
			median_sum_j += cluster4[i]->j;
			count_med++;
		}

		Point new_med_point4(median_sum_i/count_med, median_sum_j/count_med);

		median_sum_i = 0; median_sum_j = 0;
		count_med = 0;
		for(int i = 0; i<cluster5.size(); i++){
			median_sum_i += cluster5[i]->i;
			median_sum_j += cluster5[i]->j;
			count_med++;
		}

		Point new_med_point5(median_sum_i/count_med, median_sum_j/count_med);

		if(count++ == 2){
				finish_looping  = true;
			}else{
				mid_point1.i = new_med_point1.i;
				mid_point1.j = new_med_point1.j;
				mid_point2.i = new_med_point2.i;
				mid_point2.j = new_med_point2.j;
				mid_point3.i = new_med_point3.i;
				mid_point3.j = new_med_point3.j;
				mid_point4.i = new_med_point4.i;
				mid_point4.j = new_med_point4.j;
				mid_point5.i = new_med_point5.i;
				mid_point5.j = new_med_point5.j;
				cluster1.clear();
				cluster2.clear();
				cluster3.clear();
				cluster4.clear();
				cluster5.clear();
			}
	}

	cout<<mid_point1.i<<"  "<<mid_point1.j<<endl;
	cout<<mid_point2.i<<"  "<<mid_point2.j<<endl;
	cout<<mid_point3.i<<"  "<<mid_point3.j<<endl;
	cout<<mid_point4.i<<"  "<<mid_point4.j<<endl;
	cout<<mid_point5.i<<"  "<<mid_point5.j<<endl;

	Image segmented_image(256, 256, "gray");

	for(int i=0; i< cluster1.size(); i++){
		segmented_image.imggray[cluster1[i]->i][cluster1[i]->j].setGrayscale(0);
	}

	for(int i=0; i< cluster2.size(); i++){
		segmented_image.imggray[cluster2[i]->i][cluster2[i]->j].setGrayscale(63);
	}

	for(int i=0; i< cluster3.size(); i++){
		segmented_image.imggray[cluster3[i]->i][cluster3[i]->j].setGrayscale(127);
	}

	for(int i=0; i< cluster4.size(); i++){
		segmented_image.imggray[cluster4[i]->i][cluster4[i]->j].setGrayscale(191);
	}

	for(int i=0; i< cluster5.size(); i++){
		segmented_image.imggray[cluster5[i]->i][cluster5[i]->j].setGrayscale(255);
	}

	myfile.open("segmented_comb.raw", ios::out|ios::binary);
	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			myfile.put(segmented_image.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();
	
	/**************************************************end of question 2 first part***********************************************/

	/**********************************************Question 2 part 2 Texture Synthesis ****************************************/
	
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 2\\apples.raw", ios::in|ios::binary);
	Image apples(185, 124, "color", myfile);
	myfile.close();

	myfile.open("applestest.raw", ios::out|ios::binary);
	for(int i=0; i<124; i++){
		for(int j=0; j<185; j++){
			myfile.put(apples.imgcolor[i][j].getR());
			myfile.put(apples.imgcolor[i][j].getG());
			myfile.put(apples.imgcolor[i][j].getB());
		}
	}
	myfile.close();
	//we need to make a random selection of the blocks and put them together so that the image becomes twice its size
	Image apples_2(370, 248, "color");

	//we need to randomly produce a number between 0-185-blocksize, same for the height but before that we need to decide on
	//block size.. we have to try three block sizes here.. 

	//block size multiples of 185x124
	//37x31
	int block_size_row = 62;
	int block_size_col = 37;

	//create a random number
	for(int i=0; i<248; i=i+block_size_row){
		for(int j=0; j<370; j=j+block_size_col){
			unsigned int row_offset = rand() % (124-block_size_row);
			unsigned int col_offset = rand() % (185-block_size_col);

			for(int m=0; m<block_size_row; m++){
				for(int n=0; n<block_size_col; n++){
					//cout<<i+m<<"  "<<j+n<<"                                "<<row_offset+m<<"  "<<col_offset+n<<endl;
					apples_2.imgcolor[i+m][j+n].setB(apples.imgcolor[row_offset+m][col_offset+n].getB());
					apples_2.imgcolor[i+m][j+n].setG(apples.imgcolor[row_offset+m][col_offset+n].getG());
					apples_2.imgcolor[i+m][j+n].setR(apples.imgcolor[row_offset+m][col_offset+n].getR());
				}
			}
		}
	}

	myfile.open("apples_blocked_37_62.raw", ios::out|ios::binary);
	for(int i=0; i<248; i++){
		for(int j=0; j<370; j++){
			myfile.put(apples_2.imgcolor[i][j].getR());
			myfile.put(apples_2.imgcolor[i][j].getG());
			myfile.put(apples_2.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//bricks
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 2\\brick.raw", ios::in|ios::binary);
	Image brick(192, 192, "color", myfile);
	myfile.close();

	myfile.open("bricktest.raw", ios::out|ios::binary);
	for(int i=0; i<192; i++){
		for(int j=0; j<192; j++){
			myfile.put(brick.imgcolor[i][j].getR());
			myfile.put(brick.imgcolor[i][j].getG());
			myfile.put(brick.imgcolor[i][j].getB());
		}
	}
	myfile.close();
	//we need to make a random selection of the blocks and put them together so that the image becomes twice its size
	Image brick_2(192*2, 192*2, "color");

	//we need to randomly produce a number between 0-185-blocksize, same for the height but before that we need to decide on
	//block size.. we have to try three block sizes here.. 

	//block size multiples of 185x124
	//37x31
	block_size_row = 48;
	block_size_col = 48;

	//create a random number
	for(int i=0; i<384; i=i+block_size_row){
		for(int j=0; j<384; j=j+block_size_col){
			unsigned int row_offset = rand() % (192-block_size_row);
			unsigned int col_offset = rand() % (192-block_size_col);

			for(int m=0; m<block_size_row; m++){
				for(int n=0; n<block_size_col; n++){
					//cout<<i+m<<"  "<<j+n<<"                                "<<row_offset+m<<"  "<<col_offset+n<<endl;
					brick_2.imgcolor[i+m][j+n].setB(brick.imgcolor[row_offset+m][col_offset+n].getB());
					brick_2.imgcolor[i+m][j+n].setG(brick.imgcolor[row_offset+m][col_offset+n].getG());
					brick_2.imgcolor[i+m][j+n].setR(brick.imgcolor[row_offset+m][col_offset+n].getR());
				}
			}
		}
	}

	myfile.open("brick_blocked_48_48.raw", ios::out|ios::binary);
	for(int i=0; i<192*2; i++){
		for(int j=0; j<192*2; j++){
			myfile.put(brick_2.imgcolor[i][j].getR());
			myfile.put(brick_2.imgcolor[i][j].getG());
			myfile.put(brick_2.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	//weave
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 2\\weave.raw", ios::in|ios::binary);
	Image weave(192, 192, "color", myfile);
	myfile.close();

	myfile.open("weavetest.raw", ios::out|ios::binary);
	for(int i=0; i<192; i++){
		for(int j=0; j<192; j++){
			myfile.put(weave.imgcolor[i][j].getR());
			myfile.put(weave.imgcolor[i][j].getG());
			myfile.put(weave.imgcolor[i][j].getB());
		}
	}
	myfile.close();
	//we need to make a random selection of the blocks and put them together so that the image becomes twice its size
	Image weave_2(192*2, 192*2, "color");

	//we need to randomly produce a number between 0-185-blocksize, same for the height but before that we need to decide on
	//block size.. we have to try three block sizes here.. 

	//block size multiples of 185x124
	//37x31
	block_size_row = 48;
	block_size_col = 48;

	//create a random number
	for(int i=0; i<384; i=i+block_size_row){
		for(int j=0; j<384; j=j+block_size_col){
			unsigned int row_offset = rand() % (192-block_size_row);
			unsigned int col_offset = rand() % (192-block_size_col);

			for(int m=0; m<block_size_row; m++){
				for(int n=0; n<block_size_col; n++){
					//cout<<i+m<<"  "<<j+n<<"                                "<<row_offset+m<<"  "<<col_offset+n<<endl;
					weave_2.imgcolor[i+m][j+n].setB(weave.imgcolor[row_offset+m][col_offset+n].getB());
					weave_2.imgcolor[i+m][j+n].setG(weave.imgcolor[row_offset+m][col_offset+n].getG());
					weave_2.imgcolor[i+m][j+n].setR(weave.imgcolor[row_offset+m][col_offset+n].getR());
				}
			}
		}
	}

	myfile.open("weave_blocked_48_48.raw", ios::out|ios::binary);
	for(int i=0; i<192*2; i++){
		for(int j=0; j<192*2; j++){
			myfile.put(weave_2.imgcolor[i][j].getR());
			myfile.put(weave_2.imgcolor[i][j].getG());
			myfile.put(weave_2.imgcolor[i][j].getB());
		}
	}
	myfile.close();

	/*
	//we have done the naive algorithm.. now its time to do the erfos freeman quilting algorithm
	//lets take an image and then block it up depending on the block size that we choose
	//we will now create a set S of all the blocks possible 
	int image_height = 124;
	int image_width = 185;
	int x = 45; int b1=10; int y=32; int b2 = 9;
	vector<Image> Set_S;
	Image* block = new Image(x, y, "color");
	for(int i=0; i<image_height-b2; i=i+y-b2){
		for(int j=0; j<image_width-b1; j=j+x-b1){
			for(int m=0; m<y; m++){
				for(int n=0; n<x; n++){
					(*block).imgcolor[m][n].setR(apples.imgcolor[i+m][j+n].getR());
					(*block).imgcolor[m][n].setG(apples.imgcolor[i+m][j+n].getG());
					(*block).imgcolor[m][n].setB(apples.imgcolor[i+m][j+n].getB());
				}
			}
			Set_S.push_back((*block));
			block = new Image(x, y, "color");
		}
	}

	myfile.open("output.txt", ios::out);
	for(int i=0; i<Set_S.size(); i++){
		for(int m=0; m<y; m++){
			for(int n=0; n<x; n++){
				myfile<<Set_S[i].imgcolor[m][n].getR()<<Set_S[i].imgcolor[m][n].getG()<<Set_S[i].imgcolor[m][n].getB()<<" ";
			}
			myfile<<endl;
		}
		myfile<<endl<<endl;
	}
	myfile.close();

	//Now I have all the blocks.. I pick up one block at random and then start building up my quilt
	//lets first find out how many entries are there in the set S.
	cout<<Set_S.size()<<endl;  //25 its 5x5
	const int size_S = Set_S.size();

	BlockMatching *block_matching = new BlockMatching[size_S];
	
	for(int i=0; i<Set_S.size(); i++){
		//lets find the SSD's with all overlapping on the left
		Image *block = &Set_S[i];
		vector<long int> list_ssdR;
		for(int m=0; m<Set_S.size(); m++){
			Image *block_comparing = &Set_S[m];
			//now we need to compare only the left of the first block.. so we take the right of this block and compute SSD with the left
			//of the original block
			long int ssdR=0;
			for(int a=0; a<y; a++){
				for(int b=x-b1; b<x; b++){
					ssdR += pow(block->imgcolor[a][b].getR() - block_comparing->imgcolor[a][b-x+10].getR(),2);
				}
			}
			ssdR = sqrt(ssdR);
			list_ssdR.push_back(ssdR);
		}

		//to find the best match for the left side.. we go through the list and look for the index at which we have the best match
		long int min = list_ssdR[0];
		int min_index = 0;
		for(int a=0; a<list_ssdR.size(); a++){
			if(min>list_ssdR[a]){
				min = list_ssdR[a];
				min_index = a;
			}
		}

		//push this value for the left for block matching
		block_matching[i].index_right = min_index;

		list_ssdR.clear();
		//we will now do it for the right..
		for(int m=0; m<Set_S.size(); m++){
			Image *block_comparing = &Set_S[m];
			//now we need to compare only the right of the first block.. so we take the left of this block and compute SSD with the right
			//of the original block
			long int ssdR=0;
			for(int a=0; a<y; a++){
				for(int b=0; b<10; b++){
					ssdR += pow(block->imgcolor[a][b].getR() - block_comparing->imgcolor[a][x-b1+b].getR(),2);
				}
			}
			ssdR = sqrt(ssdR);
			list_ssdR.push_back(ssdR);
		}

		//to find the best match for the right side.. we go through the list and look for the index at which we have the best match
		min = list_ssdR[0];
		min_index = 0;
		for(int a=0; a<list_ssdR.size(); a++){
			if(min>list_ssdR[a]){
				min = list_ssdR[a];
				min_index = a;
			}
		}

		//push this value for the right for block matching
		block_matching[i].index_left = min_index;

		//this is now for the top and bottom.. we have to find the best matches for the top and bottom as well;
		//so that when we actually do the quilting the overlapping happens on both axes
		list_ssdR.clear();
		for(int m=0; m<Set_S.size(); m++){
			Image *block_comparing = &Set_S[m];
			//now we need to compare only the left of the first block.. so we take the right of this block and compute SSD with the left
			//of the original block
			long int ssdR=0;
			for(int a=0; a<9; a++){
				for(int b=0; b<x; b++){
					ssdR += pow(block->imgcolor[a][b].getR() - block_comparing->imgcolor[y-b2+a][b].getR(),2);
				}
			}
			ssdR = sqrt(ssdR);
			list_ssdR.push_back(ssdR);
		}

		//to find the best match for the left side.. we go through the list and look for the index at which we have the best match
		min = list_ssdR[0];
		min_index = 0;
		for(int a=0; a<list_ssdR.size(); a++){
			if(min>list_ssdR[a]){
				min = list_ssdR[a];
				min_index = a;
			}
		}

		//push this value for the left for block matching
		block_matching[i].index_top = min_index;


		//bottom
		list_ssdR.clear();
		for(int m=0; m<Set_S.size(); m++){
			Image *block_comparing = &Set_S[m];
			//now we need to compare only the left of the first block.. so we take the right of this block and compute SSD with the left
			//of the original block
			long int ssdR=0;
			for(int a=y-b2; a<y; a++){
				for(int b=0; b<x; b++){
					ssdR += pow(block->imgcolor[a][b].getR() - block_comparing->imgcolor[a-y+b2][b].getR(),2);
				}
			}
			ssdR = sqrt(ssdR);
			list_ssdR.push_back(ssdR);
		}

		//to find the best match for the left side.. we go through the list and look for the index at which we have the best match
		min = list_ssdR[0];
		min_index = 0;
		for(int a=0; a<list_ssdR.size(); a++){
			if(min>list_ssdR[a]){
				min = list_ssdR[a];
				min_index = a;
			}
		}

		//push this value for the left for block matching
		block_matching[i].index_bottom = min_index;
	}

	//we have the blocks that match the best now in the block_matching 
	//we know that for block 2 the best match on left is first block, the best match on right is the third block, and best match on bottom
	//is the 7th block
	cout<<block_matching[6].index_left<<"  "<<block_matching[6].index_right<<"  "<<block_matching[6].index_top<<"  "<<block_matching[6].index_bottom<<endl;
	
	//using just this.. lets make an image double in size
	Image apples_double(185*2, 124*2, "color");

	//we start by copying the image into the array
	for(int i=0; i<124; i++){
		for(int j=0; j<185; j++){
			apples_double.imgcolor[i][j].setB(apples.imgcolor[i][j].getB());
			apples_double.imgcolor[i][j].setG(apples.imgcolor[i][j].getG());
			apples_double.imgcolor[i][j].setR(apples.imgcolor[i][j].getR());
		}
	}
	/*
	myfile.open("apples_Double.raw", ios::out|ios::binary);
	for(int i=0; i<124*2; i++){
		for(int j=0; j<185*2; j++){
			myfile.put(apples_double.imgcolor[i][j].getR());
			myfile.put(apples_double.imgcolor[i][j].getG());
			myfile.put(apples_double.imgcolor[i][j].getB());
		}
	}
	myfile.close();
	

	//next we add the line across 4
	int index = 4;
	for(int i=0; i<4; i++){
		index = block_matching[index].index_left;
		Image *block = &Set_S[index];
		int row_a = 0; int col_a = 0;
		for(int a=0; a<y; a++){
			for(int b=185+i*x; b<185+(i+1)*x; b++){
				apples_double.imgcolor[a][b].setR(block->imgcolor[row_a][col_a].getR());
				apples_double.imgcolor[a][b].setG(block->imgcolor[row_a][col_a].getG());
				apples_double.imgcolor[a][b].setB(block->imgcolor[row_a][col_a++].getB());
			}
			col_a = 0;
			row_a++;
		}
	}


	*/
	
	/*******************************************************end of problem 2******************************************************/

	/*******************************************************problem 3 start**********************************************************/
	/*
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training1.raw", ios::in|ios::binary);
	Image training_1_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training2.raw", ios::in|ios::binary);
	Image training_2_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training3.raw", ios::in|ios::binary);
	Image training_3_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training4.raw", ios::in|ios::binary);
	Image training_4_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training5.raw", ios::in|ios::binary);
	Image training_5_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training6.raw", ios::in|ios::binary);
	Image training_6_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training7.raw", ios::in|ios::binary);
	Image training_7_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training8.raw", ios::in|ios::binary);
	Image training_8_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training9.raw", ios::in|ios::binary);
	Image training_9_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\training\\training10.raw", ios::in|ios::binary);
	Image training_10_color(200, 300, "color", myfile);
	myfile.close();

	//opening all testing files
	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing1.raw", ios::in|ios::binary);
	Image testing_1_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing2.raw", ios::in|ios::binary);
	Image testing_2_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing3.raw", ios::in|ios::binary);
	Image testing_3_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing4.raw", ios::in|ios::binary);
	Image testing_4_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing5.raw", ios::in|ios::binary);
	Image testing_5_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing6.raw", ios::in|ios::binary);
	Image testing_6_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing7.raw", ios::in|ios::binary);
	Image testing_7_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing8.raw", ios::in|ios::binary);
	Image testing_8_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing9.raw", ios::in|ios::binary);
	Image testing_9_color(200, 300, "color", myfile);
	myfile.close();

	myfile.open("C:\\Users\\Anuj_Varma\\Downloads\\Studies\\EE569\\HW3\\materials\\Problem\ 3\\testing\\testing10.raw", ios::in|ios::binary);
	Image testing_10_color(200, 300, "color", myfile);
	myfile.close();

	//call function to create binary 
	Image training1_8 = binarize(training_1_color);
	Image training2_J = binarize(training_2_color);
	Image training3_U = binarize(training_3_color);
	Image training4_9 = binarize(training_4_color);
	Image training5_C = binarize(training_5_color);
	Image training6_F = binarize(training_6_color);
	Image training7_6 = binarize(training_7_color);
	Image training8_7 = binarize(training_8_color);
	Image training9_K = binarize(training_9_color);
	Image training10_5 = binarize(training_10_color);

	//binarizing testing files
	Image testing1_K = binarize(testing_1_color);
	Image testing2_5 = binarize(testing_2_color);
	Image testing3_U = binarize(testing_3_color);
	Image testing4_J = binarize(testing_4_color);
	Image testing5_8 = binarize(testing_5_color);
	Image testing6_7 = binarize(testing_6_color);
	Image testing7_F = binarize(testing_7_color);
	Image testing8_C = binarize(testing_8_color);
	Image testing9_9 = binarize(testing_9_color);
	Image testing10_6 = binarize(testing_10_color);

	myfile.open("training1_8.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			myfile.put(training1_8.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	//all calculations for training
	ImageOCRData  training1_8_data = calculations(training1_8);
	ImageOCRData  training2_J_data = calculations(training2_J);
	ImageOCRData  training3_U_data = calculations(training3_U);
	ImageOCRData  training4_9_data = calculations(training4_9);
	ImageOCRData  training5_C_data = calculations(training5_C);
	ImageOCRData  training6_F_data = calculations(training6_F);
	ImageOCRData  training7_6_data = calculations(training7_6);
	ImageOCRData  training8_7_data = calculations(training8_7);
	ImageOCRData  training9_k_data = calculations(training9_K);
	ImageOCRData  training10_5_data = calculations(training10_5);

	writeToFile(training1_8_data, "TrainingData_1_8.txt");
	writeToFile(training2_J_data, "TrainingData_2_J.txt");
	writeToFile(training3_U_data, "TrainingData_3_U.txt");
	writeToFile(training4_9_data, "TrainingData_4_9.txt");
	writeToFile(training5_C_data, "TrainingData_5_C.txt");
	writeToFile(training6_F_data, "TrainingData_6_F.txt");
	writeToFile(training7_6_data, "TrainingData_7_6.txt");
	writeToFile(training8_7_data, "TrainingData_8_7.txt");
	writeToFile(training9_k_data, "TrainingData_9_k.txt");
	writeToFile(training10_5_data, "TrainingData_10_5.txt");

	//all calculations for testing
	ImageOCRData  testing1_K_data = calculations(testing1_K);
	ImageOCRData  testing2_5_data = calculations(testing2_5);
	ImageOCRData  testing3_U_data = calculations(testing3_U );
	ImageOCRData  testing4_J_data = calculations(testing4_J);
	ImageOCRData  testing5_8_data = calculations(testing5_8);
	ImageOCRData  testing6_7_data = calculations(testing6_7);
	ImageOCRData  testing7_F_data = calculations(testing7_F);
	ImageOCRData  testing8_C_data = calculations(testing8_C);
	ImageOCRData  testing9_9_data = calculations(testing9_9);
	ImageOCRData testing10_6_data = calculations(testing10_6);

	writeToFile(testing1_K_data, "TestingData_1_K.txt");
	writeToFile(testing2_5_data, "TestingData_2_5.txt");
	writeToFile(testing3_U_data, "TestingData_3_U.txt");
	writeToFile(testing4_J_data, "TestingData_4_J.txt");
	writeToFile(testing5_8_data, "TestingData_5_8.txt");
	writeToFile(testing6_7_data, "TestingData_6_7.txt");
	writeToFile(testing7_F_data, "TestingData_7_F.txt");
	writeToFile(testing8_C_data, "TestingData_8_C.txt");
	writeToFile(testing9_9_data, "TestingData_9_9.txt");
	writeToFile(testing10_6_data, "TestingData_10_6.txt");

	cout<<endl<<endl<<endl<<endl;
	cout<<"Please enter the path to the file you would like to test: ";
	string path_to_file;
	cin>>path_to_file;
	myfile.open(path_to_file, ios::in|ios::binary);
	Image test_file_color(200, 300, "color", myfile);
	myfile.close();

	Image test_file = binarize(test_file_color);
	myfile.open("test_test.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			myfile.put(test_file.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	ImageOCRData test_file_data = calculations(test_file);
	writeToFile(test_file_data, "Test_file_data.txt");

	if(test_file_data.EulerNumber4 == -1){
		cout<<"The number is found to be : 8"<<endl;
	}else if(test_file_data.EulerNumber4 == 0){
		//we might be dealing with either 9, 6
		float j = test_file_data.Spatial_moment_second_order.spatial_moment_x/test_file_data.area;
		float i = test_file_data.Spatial_moment_second_order.spatial_moment_y/test_file_data.area;
		float test_jpoint1 = test_file_data.Jpoints.i;
		float test_jpoint2 = test_file_data.Jpoints.j;
		float diff1 = abs(test_jpoint1- j);
		float diff2 = abs(test_jpoint2- j);
		float diff = (diff1-diff2); 
		if(diff<4){
			cout<<"The number is found to be: 9"<<endl;
		}else{
			cout<<"The number is found to be: 6"<<endl;
		}
	}else if(test_file_data.EulerNumber4 == 1){
		//J U C F 7 K 5
		if(test_file_data.symmetry_measure >0.86){
			cout<<"The number is found to be: U"<<endl;
		}else if(test_file_data.symmetry_measure<0.86 && test_file_data.symmetry_measure>0.8){
			cout<<"The number is found to be: C"<<endl;
		}else{
			float j = test_file_data.Spatial_moment_second_order.spatial_moment_x/test_file_data.area;
			float i = test_file_data.Spatial_moment_second_order.spatial_moment_y/test_file_data.area;
			float norm_area = test_file_data.area/test_file_data.bounding_box_area;
			float norm_peri = test_file_data.perimeter/test_file_data.bounding_box_peri;

			if(j>99){
				if(norm_area > 0.29 && norm_area< 0.35){
					// number is either J or 7
					if(test_file_data.symmetry_measure<0.68){
						cout<<"The number is found to be: J"<<endl;
					}else{
						cout<<"The number is found to be: 7"<<endl;
					}
				}

				if(test_file_data.circularity>0.08 && test_file_data.symmetry_measure>0.57 && test_file_data.symmetry_measure<0.75 ){
					if(test_file_data.aspect_ratio>1.6 && test_file_data.aspect_ratio<1.75){	
						cout<<"The number found to be: 5"<<endl;
					}
				}
			}else{
				if(test_file_data.circularity<0.1){
					cout<<"The number found to be: F"<<endl;
				}else{
					cout<<"The number found to be: K"<<endl;
				}
			}
		}
	}
	*/
}

Image binarize(Image trainingImage){
	Image black_white(200, 300, "gray");

	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			int sum = 0;
			sum = trainingImage.imgcolor[i][j].getB()+trainingImage.imgcolor[i][j].getG()+trainingImage.imgcolor[i][j].getR();
			sum = sum/3;
			if(sum > 100){
				black_white.imggray[i][j].setGrayscale(255);
			}else{
				black_white.imggray[i][j].setGrayscale(0);
			}
		}
	}

	for(int i=295; i<300; i++){
		for(int j=0; j<200; j++){
			black_white.imggray[i][j].setGrayscale(255);
		}
	}

	black_white.median_filtering_gray();

	return black_white;
}
float symmetry_measure(Image testing){
	//bounding box dimensions
	//if I have the bounding box dimensions.. i just need to run a reserve loop for the bounding box to get the inverted image
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(testing.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}

	//now I have the endpoints.. inversion has to happen around j
	Image inverted_image(200, 300, "gray");

	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			inverted_image.imggray[i][j].setGrayscale(255);
		}
	}

	for(int i=0; i<300; i++){
		int count = 0;
		for(int j=endPoint3.j; j>endPoint4.j-1; j--){
			inverted_image.imggray[i][endPoint4.j+count++].setGrayscale(testing.imggray[i][j].getGrayscale());
		}
	}

	fstream myfile;
	myfile.open("testing_inverted.raw", ios::out|ios::binary);
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			myfile.put(inverted_image.imggray[i][j].getGrayscale());
		}
	}
	myfile.close();

	//compare the inverted image within the bounding box;
	int num_sim_pixel = 0;
	for(int i=endPoint2.i; i<endPoint1.i; i++){
		for(int j=endPoint4.j; j<endPoint3.j; j++){
			if(inverted_image.imggray[i][j].getGrayscale() == testing.imggray[i][j].getGrayscale()){
				num_sim_pixel++;
			}
		}
	}

	float bb_area = abs((float)(endPoint1.i-endPoint2.i)*(float)(endPoint3.j-endPoint4.j));
	float symm_measure = num_sim_pixel/bb_area;

	return symm_measure;
}

void writeToFile(ImageOCRData Data, string filename){
	fstream myfile;
	myfile.open(filename, ios::out);
	myfile<<"This file contains training data for "<<filename<<endl<<endl;
	myfile<<"Area: "<<Data.area<<endl;
	myfile<<"Aspect Ratio: "<<Data.aspect_ratio<<endl;
	myfile<<"Circularity: "<<Data.circularity<<endl;
	myfile<<"Euler Number (4 connectivity): "<<Data.EulerNumber4<<endl;
	myfile<<"Euler Number (8 connectivity): "<<Data.EulerNumber8<<endl;
	myfile<<"Perimeter: "<<Data.perimeter<<endl;
	myfile<<"Spatial Moment ( M(1,0) ) x_direction:  "<<Data.Spatial_moment_second_order.spatial_moment_x<<endl;
	myfile<<"Spatial Moment ( M(0,1) ) y_direction:  "<<Data.Spatial_moment_second_order.spatial_moment_y<<endl;
	myfile<<"Bounding box area: "<<Data.bounding_box_area<<endl;
	myfile<<"Bounding box perimeter: "<<Data.bounding_box_peri<<endl;
	myfile<<"Symmetry: "<<Data.symmetry_measure<<endl;
	myfile<<"//center of gravity ="<<endl;
	myfile<<"Center of gravity: "<<Data.Spatial_moment_second_order.spatial_moment_x/Data.area<<" "<<Data.Spatial_moment_second_order.spatial_moment_y/Data.area<<endl;
	myfile<<endl<<"Normalized area: "<<Data.area/Data.bounding_box_area<<endl;
	myfile<<"Normalized perimeter: "<<Data.perimeter/Data.bounding_box_peri<<endl;
	myfile<<endl<<endl;
}

SpatialMoment Spatial_Moment(Image training){
	long float spatial_moment_x = 0;
	long float spatial_moment_y = 0;
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
			}else{
				spatial_moment_x += j*1;
				spatial_moment_y += i*1;
			}
		}
	}

	SpatialMoment sp;
	sp.spatial_moment_x = spatial_moment_x;
	sp.spatial_moment_y = spatial_moment_y;

	return sp;
}

float aspect_ratio(Image training){
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}
	
	float aspect = abs((float)(endPoint1.i-endPoint2.i)/(float)(endPoint3.j-endPoint4.j));
	return aspect;
}

Point boundingbox_i_values(Image training){
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}
	
	Point IPoints(endPoint1.i, endPoint2.i);
	return IPoints;
}

Point boundingbox_j_values(Image training){
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}
	
	Point JPoints(endPoint3.j, endPoint4.j);
	return JPoints;
}


float boundingbox_perimeter(Image training){
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}
	
	float aspect = abs(2*(abs((float)(endPoint1.i-endPoint2.i))+abs((float)(endPoint3.j-endPoint4.j))));
	return aspect;
}

float boundingbox_area(Image training){
	Point endPoint1;
	Point endPoint2;
	Point endPoint3;
	Point endPoint4;
	int maxi = 0, mini = 300, minj = 300, maxj = 0;
	//find the coordinates of the endpoints of the image
	for(int i=0; i<300; i++){
		for(int j=0; j<200; j++){
			if(training.imggray[i][j].getGrayscale() == 255){
				//do nothing
				continue;
			}else{
				if(maxi<i){
					maxi = i;
					endPoint1.i= i;
					endPoint1.j= j;
				}
				if(mini>=i){
					mini=i;
					endPoint2.i = i;
					endPoint2.j = j;
				}
				if(maxj<=j){
					maxj = j;
					endPoint3.i = i;
					endPoint3.j = j;
				}
				if(minj>j){
					minj = j;
					endPoint4.i = i;
					endPoint4.j = j;
				}
			}
		}
	}
	
	float aspect = abs((float)(endPoint1.i-endPoint2.i)*(float)(endPoint3.j-endPoint4.j));
	return aspect;
}

ImageOCRData calculations(Image training){
	//bit Quads
	int Q0[2][2] = {
		{255, 255},
		{255, 255}
	};

	//Q1
	int Q1_1[2][2] = {
		{0, 255},
		{255, 255}
	};

	int Q1_2[2][2] = {
		{255, 0},
		{255, 255}
	};

	int Q1_3[2][2] = {
		{255, 255},
		{0, 255}
	};

	int Q1_4[2][2] = {
		{255, 255},
		{255, 0}
	};

	//Q2
	int Q2_1[2][2] = {
		{0, 0},
		{255, 255}
	};

	int Q2_2[2][2] = {
		{255, 0},
		{255, 0}
	};

	int Q2_3[2][2] = {
		{255, 255},
		{0, 0}
	};

	int Q2_4[2][2] = {
		{0, 255},
		{0, 255}
	};

	//Q3
	int Q3_1[2][2] = {
		{0, 0},
		{255, 0}
	};

	int Q3_2[2][2] = {
		{255, 0},
		{0, 0}
	};

	int Q3_3[2][2] = {
		{0, 255},
		{0, 0}
	};

	int Q3_4[2][2] = {
		{0, 0},
		{0, 255}
	};

	//Q4
	int Q4[2][2] = {
		{0, 0},
		{0, 0}
	};

	//Qd
	int Qd_1[2][2] = {
		{255, 0},
		{0, 255}
	};

	int Qd_2[2][2] = {
		{0, 255},
		{255, 0}
	};

	//The quads are decided.. now we need to count them.. 
	
	int count_training_Q0 = count(training, Q0);
	int count_training_Q1 = count(training, Q1_1);
	count_training_Q1 += count(training, Q1_2);
	count_training_Q1 += count(training, Q1_3);
	count_training_Q1 += count(training, Q1_4);
	int count_training_Q2 = count(training, Q2_1);
	count_training_Q2 += count(training, Q2_2);
	count_training_Q2 += count(training, Q2_3);
	count_training_Q2 += count(training, Q2_4);
	int count_training_Q3 = count(training, Q3_1);
	count_training_Q3 += count(training, Q3_2);
	count_training_Q3 += count(training, Q3_3);
	count_training_Q3 += count(training, Q3_4);
	int count_training_Q4 = count(training, Q4);
	int count_training_Qd = count(training, Qd_1);
	count_training_Qd += count(training, Qd_2);

	float area = (1/4.0)*count_training_Q1+ (1/2.0)*count_training_Q2+ (7/8.0)*count_training_Q3+ count_training_Q4+ (3/4.0)*count_training_Qd;
	float perimeter = count_training_Q2+ (1/sqrtf(2))*(count_training_Q1+count_training_Q2+2*count_training_Qd);
	int Euler_No_4 = (1/4.0)*(count_training_Q1-count_training_Q3+2*count_training_Qd);
	int Euler_No_8 = (1/4.0)*(count_training_Q1-count_training_Q3-2*count_training_Qd);
	float circularity = (4*3.14159*area)/(pow(perimeter,2));
	float aspect = aspect_ratio(training);
	float bb_area = boundingbox_area(training);
	float bb_peri = boundingbox_perimeter(training);
	SpatialMoment spatial_moment = Spatial_Moment(training);
	float sym_measure = symmetry_measure(training);
	Point ipoints = boundingbox_i_values(training);
	Point jpoints = boundingbox_j_values(training);
	ImageOCRData OCRtrainingData(area, perimeter, Euler_No_4, Euler_No_8, circularity, spatial_moment, aspect, bb_area, bb_peri, sym_measure, ipoints, jpoints);
	return OCRtrainingData;
}

int count(Image training, int Q[][2]){
	int count_total = 0;
	
	for(int i=1; i<299; i++){
		for(int j=1; j<199; j++){
			
			int temp[2][2] = {
				{0, 0},
				{0, 0}
			};

			for(int m=0; m<2; m++){
				for(int n=0; n<2; n++){
					temp[m][n] = training.imggray[i+m][j+n].getGrayscale();
				}
			}
			

			int count = 0;
			for(int l=0; l<2; l++){
				for(int k=0; k<2; k++){
					if(temp[l][k] == Q[l][k]){
						count++;
					}
				}
			}

			if(count == 4){
				//all four values matched
				count_total++;
			}
		}
	}
	return count_total;
}

Point* mid_point_function(list<Point*> list_of_points, int size_of_list){
	list<Point*>::iterator itr;
	Point* mid_point = new Point();
	int sum_x, sum_y;
	sum_x = sum_y = 0;
	int num = 0;
	for(itr = list_of_points.begin(); itr != list_of_points.end(); itr++){
		sum_x += (*itr)->j;
		sum_y += (*itr)->i;
		num++;
	}
	mid_point->i = sum_y/num;
	mid_point->j = sum_x/num;

	return mid_point;
}

float distance_points(int x1, int y1, int x2, int y2){
	float sum_of_squares = pow((x1-x2),2) + pow((y1- y2),2);
	float dist = sqrt(sum_of_squares);

	return dist;
}

bool triangle_inside_test(vector<Point*> triangle, int x, int y){
	//calculate the sides of the triangle
	float side1 = distance_points(triangle[0]->i, triangle[0]->j, triangle[1]->i, triangle[1]->j);
	float side2 = distance_points(triangle[0]->i, triangle[0]->j, triangle[2]->i, triangle[2]->j);
	float side3 = distance_points(triangle[2]->i, triangle[2]->j, triangle[1]->i, triangle[1]->j);

	//semi-perimeter of triangle
	float semi_perim = (side1+side2+side3)/2;
	float area_sq = semi_perim*(semi_perim-side1)*(semi_perim-side2)*(semi_perim-side3);
	float area = sqrt(area_sq);

	float point_side1_triangle1 = distance_points(triangle[0]->i, triangle[0]->j, x, y);
	float point_side2_triangle1 = distance_points(triangle[0]->i, triangle[0]->j, triangle[1]->i, triangle[1]->j);
	float point_side3_triangle1 = distance_points(triangle[1]->i, triangle[1]->j, x, y);

	float point_side1_triangle2 = distance_points(triangle[0]->i, triangle[0]->j, x, y);
	float point_side2_triangle2 = distance_points(triangle[0]->i, triangle[0]->j, triangle[2]->i, triangle[2]->j);
	float point_side3_triangle2 = distance_points(triangle[2]->i, triangle[2]->j, x, y);

	float point_side1_triangle3 = distance_points(triangle[2]->i, triangle[2]->j, x, y);
	float point_side2_triangle3 = distance_points(triangle[2]->i, triangle[2]->j, triangle[1]->i, triangle[1]->j);
	float point_side3_triangle3 = distance_points(triangle[1]->i, triangle[1]->j, x, y);

	float semi_perim1 = (point_side1_triangle1+point_side2_triangle1+point_side3_triangle1)/2;
	float area_sq1 = semi_perim1*(semi_perim1-point_side1_triangle1)*(semi_perim1-point_side2_triangle1)*(semi_perim1-point_side3_triangle1);
	float area1 = sqrt(area_sq1);

	float semi_perim2 = (point_side1_triangle2+point_side2_triangle2+point_side3_triangle2)/2;
	float area_sq2 = semi_perim2*(semi_perim2-point_side1_triangle2)*(semi_perim2-point_side2_triangle2)*(semi_perim2-point_side3_triangle2);
	float area2 = sqrt(area_sq2);

	float semi_perim3 = (point_side1_triangle3+point_side2_triangle3+point_side3_triangle3)/2;
	float area_sq3 = semi_perim3*(semi_perim3-point_side1_triangle3)*(semi_perim3-point_side2_triangle3)*(semi_perim3-point_side3_triangle3);
	float area3 = sqrt(area_sq3);

	bool inside_triangle = false;
	if((area1+area2+area3)-area <1 && (area1+area2+area3)-area > -1){
		inside_triangle = true;
	} 

	return inside_triangle;
}

int triangleNumber(int x, int y){
	//lets start by defining our triangles which can be a vector of point 3
	vector<Point*> triangle1;
	vector<Point*> triangle2;
	vector<Point*> triangle3;
	vector<Point*> triangle4;

	//for this version of point lets stick with i=x and j=y
	triangle1.push_back(new Point(0,0));
	triangle1.push_back(new Point(400,0));
	triangle1.push_back(new Point(203,201));

	triangle2.push_back(new Point(0,0));
	triangle2.push_back(new Point(203,201));
	triangle2.push_back(new Point(0,500));

	triangle3.push_back(new Point(0,500));
	triangle3.push_back(new Point(400,500));
	triangle3.push_back(new Point(203,201));

	triangle4.push_back(new Point(203,201));
	triangle4.push_back(new Point(400,0));
	triangle4.push_back(new Point(400,500));

	//test point with triangle 1
	int inside_which_triangle = 0;

	if(triangle_inside_test(triangle1, x, y)){
		inside_which_triangle = 1;
	}else if(triangle_inside_test(triangle2, x, y)){
		inside_which_triangle = 2;
	}else if(triangle_inside_test(triangle3, x, y)){
		inside_which_triangle = 3;
	}else if(triangle_inside_test(triangle4, x, y)){
		inside_which_triangle = 4;
	}

	return inside_which_triangle;
}

vector< vector<int> > matrix_multiply(vector< vector<int> > matrix1, vector< vector<int> > matrix2){
	int row_size1 = matrix1.size();
	int column_size1 = matrix1[0].size();

	int row_size2 = matrix2.size();
	int column_size2 = matrix2[0].size();

	if(column_size1 != column_size2){
		cout<<"Matrix dimensions do not match for multiplication"<<endl;
		exit(1);
	}
	//create a new inverted matrix2
	vector< vector<int> > matrix2_invert;
	matrix2_invert.resize(column_size2);
	for(int i=0; i<column_size2; i++){
		matrix2_invert[i].resize(row_size2);
	}
	//fill up matrix2_invert 
	for(int i = 0; i<row_size2; i++){
		for(int j=0; j<column_size2; j++){
			matrix2_invert[j][i] = matrix2[i][j];
		}
	}

	//to store the result
	vector< vector<int> > matrix_result;
	matrix_result.resize(row_size1);
	for(int i=0; i<row_size1; i++){
		matrix_result[i].resize(row_size2);
	}

	//mutiply matrix 1 with matrix2_invert
	for(int i=0; i<row_size1; i++){
		for(int j=0; j<row_size2; j++){
			int sum = 0;
			for(int k=0; k<column_size1; k++){
				sum+=matrix1[i][k]*matrix2_invert[k][j];
			}
			matrix_result[i][j] = sum;
		}
	}
	
	return matrix_result;
}

Image filtered_image(Image comb, vector< vector<int> > matrix){
	Image filter_result(256, 256, "gray");
	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			int sum = 0; int row, column;
			for(int m=-2; m<3; m++){
				for(int n=-2; n<3; n++){
					if(i+m<0){
						row = (i+m)*(-1) - 1;
					}else if(i+m>255){
						row = 255-((i+m) - 256);
					}else{
						row = i+m;
					}

					if(j+n<0){
						column = (j+n)*(-1) - 1;
					}else if(j+n>255){
						column = 255-((j+n) - 256);
					}else{
						column = j+n;
					}
					sum += comb.imggray[row][column].getGrayscale()*matrix[m+2][n+2];
				}
			}
			filter_result.imggray[i][j].setGrayscale(sum);
		}
	}

	return filter_result;
}

void windowing(Image filter, float** window_result){
	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			long float sum = 0; int row, column;
			for(int m=-7; m<8; m++){
				for(int n=-7; n<8; n++){
					if(i+m<0){
						row = (i+m)*(-1) - 1;
					}else if(i+m>255){
						row = 255-((i+m) - 256);
					}else{
						row = i+m;
					}

					if(j+n<0){
						column = (j+n)*(-1) - 1;
					}else if(j+n>255){
						column = 255-((j+n) - 256);
					}else{
						column = j+n;
					}
					sum = sum+pow(filter.imggray[row][column].getGrayscale(),2.0);
				}
			}

			if(sum <0){
				cout<<"Sum is less than zero"<<endl;
				cout<<filter.imggray[row][column].getGrayscale()<<endl;
				cout<<pow(filter.imggray[row][column].getGrayscale(),2.0)<<endl;
				cout<<i<<"  "<<j<<endl;
				exit(1);
			}
			window_result[i][j] = sqrtf(sum);
		}
	}
}

void normalize(float** matrix1, float** matrix2){
	for(int i=0; i<256; i++){
		for(int j=0; j<256; j++){
			matrix1[i][j] = matrix1[i][j]/matrix2[i][j];
		}
	}
}