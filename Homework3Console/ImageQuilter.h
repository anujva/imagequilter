#pragma once
#include<iostream>
#include<cstdlib>
#include"Image.h"
#include "View.h"
#include "Patch.h"
#include "MinPathFinder.h"
#include "SynthAide.h"

class ImageQuilter{
private:
	Image *inputImage;
	int patchsize;
	int overlapsize;
	bool allowHorizontalPaths;
	double pathCostWeight;
	int inputImageWidth;
	int inputImageHeight;
	double getOverlapDistDifference(Patch *outPatch, TwoDLoc loc);
	double avgCostOfBestPath(vector< vector<double> > leftoverlap, vector< vector<double> > topoverlap);
	vector< vector<double> > getTopOverlapDists(Patch *outPatch, Patch *inPatch);
	vector< vector<double> > getLeftOverlapDists(Patch *outPatch, Patch *inPatch);
	void fillAndBlend(Patch *outPatch, TwoDLoc loc);
	void pathAndFill(Patch *outPatch, TwoDLoc loc);
	void followLeftOverlapPath(Patch *toPatch, Patch *fromPatch,
				 MinPathFinder finder, TwoDLoc source);
	void followTopOverlapPath(Patch *toPatch, Patch *fromPatch,
				 MinPathFinder finder, TwoDLoc source);
	void choosePathIntersection(MinPathFinder *leftpath, MinPathFinder *toppath, TwoDLoc leftloc, TwoDLoc toploc);

public:
	ImageQuilter(void);
	~ImageQuilter(void);

	ImageQuilter(Image *input, int patch, int overlap, bool allowHor, double pathCost);

	Image* synthesize(int outwidth, int outheight);
	TwoDLoc calcDists(double **dists, Patch* outPatch)
};

