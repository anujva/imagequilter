#pragma once

#include "TwoDLoc.h"

class MinPathFinder{
private:
	double **costs;
	TwoDLoc **path;
	int rowcnt;
	int colcnt;

public:
	MinPathFinder(void);
	~MinPathFinder(void);
	MinPathFinder(double ** dists, bool allowHorizontal, int distlength1, int distlength2);
	TwoDLoc follow(TwoDLoc currentLoc);
	TwoDLoc bestSourceLoc();
	double costOf(int row, int col);
	TwoDLoc** getPaths();
	double** getCosts();
	void handleHorizontalMovement(double *dists, int row);

};

