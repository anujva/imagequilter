This file contains training data for TestingData_10_6.txt

Area: 7024.75
Aspect Ratio: 1.47619
Circularity: 0.0917516
Euler Number (4 connectivity): 0
Euler Number (8 connectivity): 0
Perimeter: 980.874
Spatial Moment ( M(1,0) ) x_direction:  685498
Spatial Moment ( M(0,1) ) y_direction:  996557
Bounding box area: 16275
Bounding box perimeter: 520
Symmetry: 0.758525
//center of gravity =
Center of gravity: 97.5833 141.864

Normalized area: 0.431628
Normalized perimeter: 1.8863


