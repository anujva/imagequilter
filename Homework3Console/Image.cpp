#include "Image.h"


Image::Image(void){

}


Image::~Image(void){

}

Image::Image(int _width, int _height, string ImageType){
	width = _width;
	height = _height;

	if(!ImageType.compare("color")){
		imgcolor = new ImageColor*[height];
	
		for(int i=0; i<height; i++){
			imgcolor[i] = new ImageColor[width];
		}
	}else if(!ImageType.compare("gray")){
		imggray = new ImageGray*[height];

		for(int i=0; i<height; i++){
			imggray[i] = new ImageGray[width];
		}

		//initialize all values of imggray to zero
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				imggray[i][j].setGrayscale(0);
			}
		}
	}
}



Image::Image(int _width, int _height, string ImageType, fstream &myfile){
	width = _width;
	height = _height;

	if(!ImageType.compare("color")){
		imgcolor = new ImageColor*[height];
	
		for(int i=0; i<height; i++){
			imgcolor[i] = new ImageColor[width];
		}

		int i = 0; int j = 0;
		while(!myfile.eof()){
			imgcolor[i][j].setR((int)myfile.get());
			imgcolor[i][j].setG((int)myfile.get());
			imgcolor[i][j].setB((int)myfile.get());
			j++;
			if(j == width){
				j = 0;
				i++;
			}
			if(i == height) break;
		}
	}else if(!ImageType.compare("gray")){
		imggray = new ImageGray*[height];

		for(int i=0; i<height; i++){
			imggray[i] = new ImageGray[width];
		}

		//initialize all values of imggray to zero
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				imggray[i][j].setGrayscale(0);
			}
		}

		int i = 0; int j = 0;
		while(!myfile.eof()){
			imggray[i][j].setGrayscale((int)myfile.get());
			j++;
			if(j == width){
				j = 0;
				i++;
			}
			if(i == height) break;
		}
	}
}

void Image::median_filtering_gray(){
	//this filter will work on the image to remove salt and pepper noise from the image.
	vector<int> neighborhood;
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			for(int k=-2; k<3; k++){
				for(int l=-2; l<3; l++){
					if(i+k<0 || i+k>(height-1) || j+l<0 || j+l > (width-1)){
						neighborhood.push_back(imggray[i][j].getGrayscale());
					}else{
						//we have to create an array with all the 9 entries and then sort them.
						neighborhood.push_back(imggray[i+k][j+l].getGrayscale());
					}
				}
			}
			//now that the neighbourhood has been filled.. its time to sort them.. and find the middle value.. 
			sort(neighborhood.begin(), neighborhood.end());
			//cout<<neighborhood[(neighborhood.size())/2]<<"   ";
			imggray[i][j].setGrayscale(neighborhood[(neighborhood.size())/2]);
			neighborhood.clear();
		}
	}
}

void Image::mean_filtering_gray(){
	vector<int> neighborhood;
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			for(int k=-1; k<2; k++){
				for(int l=-1; l<2; l++){
					if(i+k<0 || i+k>511 || j+l<0 || j+l > 511){
						neighborhood.push_back(imggray[i][j].getGrayscale());
					}else{
						//we have to create an array with all the 9 entries and then sort them.
						neighborhood.push_back(imggray[i+k][j+l].getGrayscale());
					}
				}
			}
			//now that the neighbourhood has been filled.. its time to average them up..
			int sum = 0;
			for(int m = 0; m<neighborhood.size(); m++){
				sum += neighborhood[m];
			}
			imggray[i][j].setGrayscale(sum/neighborhood.size());
			neighborhood.clear();
		}
	}
}

ImageGray** Image::digital_histogram_eq_gray(){
	int image_no_of_pixels = width * height;
	int quantized_level = image_no_of_pixels/256;

	//store the positions in a linear array of size no of pixels;
	vector<Coordinates *> store_of_position;

	for(int i = 0; i< 256; i++){
		for(int l = 0; l<height; l++){
			for(int k = 0; k<width; k++){
				if( i == imggray[l][k].getGrayscale()){
					store_of_position.push_back(new Coordinates(l,k));
				}
			}
		}
	}

	ImageGray **new_equalized;
	new_equalized = new ImageGray*[height];

	for(int i=0; i<height; i++){
		new_equalized[i] = new ImageGray[width];
	}

	int block_pixel = 0, iteration_no = 1;
	vector<Coordinates *>::iterator itr;
	for(itr = store_of_position.begin(); itr!=store_of_position.end(); itr++){
		//cout<<"Position vector row: "<<(*itr)->r<<" column: "<<(*itr)->c<<endl;
		if(iteration_no%quantized_level == 0){
			block_pixel++;
		}
		if(block_pixel>255){
			block_pixel = 255;
		}
		new_equalized[(*itr)->r][(*itr)->c].setGrayscale(block_pixel);
		iteration_no++;
	}
	//cout<<"The total number of iterations the loop went through = "<<iteration_no<<endl;
	return new_equalized;
}

ImageGray** Image::filterImage(int type){
	filter = new LowPassFiltering(type, imggray, 512, 512);
	return imggray;
}

Image Image::SSIM_calculation(Image B, string str){
	Image SSIM_Map(width, height, "gray");
	float SSIM_index_whole = 0;
	for(int i=0; i<height; i++){
			for(int j = 0; j<width; j++){
				int sumA = 0;
				int sumB = 0;
				int uA = 0, uB = 0; 
				float varB = 0, varA = 0, varAB = 0;
				int row_index =0, column_index = 0;
				float temp = 0;
				//calculating the mean for A and B images
				for(int a=-2; a<3; a++){
					for(int b =-2; b <3; b++){
						if(a+i < 0 || a+i>(height-1) || j+b<0 || j+b>(width-1)){
							//sum = sum + filter[a+i][b+j]*ImageSet[i][j].getGrayscale();
							if(a+i<0){
								row_index = -1*(a+i)-1;
							}else if(a+i > (height -1)){
								row_index =(height-1) - (a+i - (height-1))+1;
							}else{
								row_index = a+i;
							}

							if(b+j<0){
								column_index = -1*(b+j)-1;
							}else if(b+j > (width -1)){
								column_index =(width-1) - (b+j - (width-1))+1;
							}else{
								column_index = b+j;
							}
							sumA = sumA + imggray[row_index][column_index].getGrayscale();
							sumB = sumB + B.imggray[row_index][column_index].getGrayscale();
							continue;
						}
						sumA = sumA + imggray[i+a][j+b].getGrayscale();
						sumB = sumB + B.imggray[i+a][j+b].getGrayscale();
					}
				}
				uA = sumA/25;
				uB = sumB/25;

				//mean has been calculated for both the images. Now we need to calculate the variance for each and the covariance
				sumA = 0; sumB = 0;
				int sumAB = 0;
				for(int a=-2; a<3; a++){
					for(int b =-2; b <3; b++){
						if(a+i < 0 || a+i>(height-1) || j+b<0 || j+b>(width-1)){
							//sum = sum + filter[a+i][b+j]*ImageSet[i][j].getGrayscale();
							if(a+i<0){
								row_index = -1*(a+i)-1;
							}else if(a+i > (height -1)){
								row_index =(height-1) - (a+i - (height-1))+1;
							}else{
								row_index = a+i;
							}

							if(b+j<0){
								column_index = -1*(b+j)-1;
							}else if(b+j > (width -1)){
								column_index =(width-1) - (b+j - (width-1))+1;
							}else{
								column_index = b+j;
							}
							sumA = sumA + pow((imggray[row_index][column_index].getGrayscale()-uA),2);
							sumB = sumB + pow((B.imggray[row_index][column_index].getGrayscale()-uB),2);
							sumAB = sumAB + (imggray[row_index][column_index].getGrayscale()-uA)*(B.imggray[row_index][column_index].getGrayscale()-uB);
							continue;
						}
						sumA = sumA + pow((imggray[a+i][j+b].getGrayscale()-uA),2);
						sumB = sumB + pow((B.imggray[a+i][j+b].getGrayscale()-uB),2);
						sumAB = sumAB + (imggray[a+i][j+b].getGrayscale()-uA)*(B.imggray[a+i][b+j].getGrayscale()-uB);
					}
				}
				varA = sumA/25.0;
				varB = sumB/25.0;
				varAB = sumAB/25.0;

				//now we have all the values that we need.. and we just need to put in the value for the ssim in a template
				temp =  ((2*uA*uB+6.5025)*(2*varAB+58.5225))/((pow(uA,2)+pow(uB,2)+6.5025)*(varA+varB+58.5225));
				//cout<<uA<<"    "<<uB<<"    "<<varA<<"    "<<varB<<"    "<<varAB<<"    "<<temp<<endl;
				int ssim_index = (int)((temp+1)*255/2);
				
				if(temp > 1 || temp < -1){
					cout<<"We have some error in the formula somewhere"<<endl;
				}

				SSIM_Map.imggray[i][j].setGrayscale(ssim_index);
				SSIM_index_whole += temp;
			}
		}

	SSIM_index_whole = SSIM_index_whole/(height*width);

	cout<<str<<":   "<<SSIM_index_whole<<endl;
	//SSIM_Map has been generated.. need to print it out.. so we will return it
	return SSIM_Map;
}

Image Image::PSNR_Calculation(Image B){
	//Image B is the image that will have the pixels for which we will be calculating the error rate
	Image AE_map(width, height, "gray");
	float MSE = 0;
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			int diff = B.imggray[i][j].getGrayscale()-imggray[i][j].getGrayscale();
			MSE += pow((double)diff,2.0);
			if(diff<0){
				diff *= -1;
			}
			AE_map.imggray[i][j].setGrayscale (diff);
		}
	}
	MSE = (float)(1.0/width)*(float)(1.0/height)*MSE;
	cout<<endl<<"*****************************************************************"<<endl;
	cout<< "The Mean squared error is : "<<MSE<<endl;

	float PSNR = 0;
	PSNR = 10*log10(pow(255.00,2)/MSE);

	cout<<endl<<"*****************************************************************"<<endl;
	cout<<"The PSNR of the image is : "<<PSNR<<endl;
	return AE_map;
}

Image Image::Sobel_Operator(string which_edge){
	Image Sobel_edge_map_Gx(width, height, "gray");
	Image Sobel_edge_map_Gy(width, height, "gray");
	Image Sobel_edge_map(width, height, "gray");
	int sobel_op_x[3][3] = {
		{-1, 0, +1},
		{-2, 0, +2},
		{-1, 0, +1}
	};

	int sobel_op_y[3][3] = {
		{-1, -2, -1},
		{0, 0, 0},
		{+1, +2, +1}
	};

	for(int i = 0; i<height; i++){
		for(int j = 0; j<width; j++){
			int row_index = 0, column_index = 0;
			int sum = 0, sumGx = 0, sumGy = 0;
			for(int k=-1; k<2; k++){
				for(int l=-1; l<2; l++){
					//Gx && Gy
					if(i+k<0 || i+k>height-1 || j+l<0 || j+l >width-1){
						if(i+k<0){
							row_index = (i+k)*-1 - 1;
						}else if(i+k > height -1){
							row_index = i+k - 1;
						}

						if(j+l<0){
							column_index = -1*(j+l) - 1;
						}else if(j+l> width-1){
							column_index = j+l-1;
						}
						sumGx += sobel_op_x[k+1][l+1]*imggray[row_index][column_index].getGrayscale();
						sumGy += sobel_op_y[k+1][l+1]*imggray[row_index][column_index].getGrayscale();
					}else{
						sumGx += sobel_op_x[k+1][l+1]*imggray[i+k][j+l].getGrayscale();
						sumGy += sobel_op_y[k+1][l+1]*imggray[i+k][j+l].getGrayscale();
					}
				}
			}
			//sumGx = (1/4) * sumGx;
			//sumGy = (1/4) * sumGy;
			sum = (int)pow((pow(sumGx,2) + pow(sumGy,2)), 0.5);
			if(sumGx > 255){
				sumGx = 255;
			}else if(sumGx < 0){
				sumGx = 0;
			}

			if(sumGy > 255){
				sumGy = 255;
			}else if(sumGy < 0){
				sumGy = 0;
			}

			if(sum>255){
				sum = 255;
			}else if(sum < 0){
				sum =0;
			}
			

			//tuning the threshold in terms of percentages
			//first we will need to get the histogram 
			if(sumGx > 40){
				sumGx = 255;
			}else{
				sumGx = 0;
			}

			if(sumGy > 40){
				sumGy = 255;
			}else{
				sumGy = 0;
			}

			if(sum > 40){
				sum = 255;
			}else{
				sum = 0;
			}
			Sobel_edge_map_Gx.imggray[i][j].setGrayscale(sumGx);
			Sobel_edge_map_Gy.imggray[i][j].setGrayscale(sumGy);
			Sobel_edge_map.imggray[i][j].setGrayscale(sum);
		}
	}
	if(which_edge .compare("full") == 0){
		return Sobel_edge_map;
	}else if(which_edge.compare("gx") == 0){
		return Sobel_edge_map_Gx;
	}else if(which_edge.compare("gy") ==0){
		return Sobel_edge_map_Gy;
	}
}

Image Image::Edge_Detector_LOG(string str){
	Image Edge_Detector_map(width, height, "gray");
	Image Zero_Crossing_map(width, height, "gray");

	//We need to do a few calculations in order to find the LoG kernel 
	int LOG_kernel[5][5] = {
		{0,0,1,0,0},
		{0,1,2,1,0},
		{1,2,-16,2,1},
		{0,1,2,1,0},
		{0,0,1,0,0}
	};

	int printrow = 0;
	for(int i=0; i<height; i++){
		for(int j = 0; j<width; j++){
			int row_index = 0, column_index = 0;
			int sum = 0;
			for(int k=-2; k<3; k++){
				for(int l=-2; l<3; l++){
					if(i+k <0 || i+k > height -1 || j+l<0 || j+l>width -1){
						if(i+k < 0){
							row_index = (i+k)*-1 -1;
						}else if(i+k > height -1){
							row_index = (height - 1) - (i+k - (height - 1)) + 1;
						}else{
							row_index = i+k;
						}

						if(j+l < 0){
							column_index = (j+l)*-1 -1;
						}else if(i+k > width -1){
							column_index = (width - 1) - (j+l - (width - 1)) + 1;
						}else{
							column_index = j+l;
						}
						sum += LOG_kernel[k+2][l+2] * imggray[row_index][column_index].getGrayscale();
					}else{
						sum += LOG_kernel[k+2][l+2] * imggray[i+k][j+l].getGrayscale();
					}
				}		
			}
			printrow++;
			/*
			if(printrow%30 == 0){
				cout<<endl;
			}else{
				cout<<sum<<"   ";
			}
			*/
			Zero_Crossing_map.imggray[i][j].setGrayscale(sum);
		}
	}

	/*
	//Inspect the zero crossing map for zero crossings.. We will look at present value and old value and check to see if it has a zero crossing. we will threshold this 
	//a bit as well and only say its an edge if the value is strong
	int diff = 0;
	for(int i = 0; i<height; i++){
		for(int j = 0; j<width; j++){
			//compare columns for edges
			if(j>0){
				if(Zero_Crossing_map.imggray[i][j].getGrayscale() > 0 && Zero_Crossing_map.imggray[i][j-1].getGrayscale() <= 0){
					diff = Zero_Crossing_map.imggray[i][j].getGrayscale() - Zero_Crossing_map.imggray[i][j-1].getGrayscale();
					if(diff > 80 || diff <-80){
						//cout<<"Inside if #1  "<<diff<<endl;
						Edge_Detector_map.imggray[i][j].setGrayscale(255);
					}
				}else if(Zero_Crossing_map.imggray[i][j].getGrayscale() < 0 && Zero_Crossing_map.imggray[i][j-1].getGrayscale() >= 0){
					diff = Zero_Crossing_map.imggray[i][j].getGrayscale() - Zero_Crossing_map.imggray[i][j-1].getGrayscale();
					if(diff > 80 || diff <-80){
						//cout<<"Inside if #2  "<<diff<<endl;
						Edge_Detector_map.imggray[i][j].setGrayscale(255);
					}
				}else{
					//cout<<"Inside if else  "<<endl;
					Edge_Detector_map.imggray[i][j].setGrayscale(0);
				}
			}else {
				Edge_Detector_map.imggray[i][j].setGrayscale(0);
			}
		}
	}
	*/

	for(int i = 0; i<height; i++){
		for(int j =0 ; j<height; j++){
			if(Zero_Crossing_map.imggray[i][j].getGrayscale() > 80){
				Edge_Detector_map.imggray[i][j].setGrayscale(255);
			}
		}
	}

	return Edge_Detector_map;
}

Image Image::Variance_map(string str){
	Image Variance_map(width, height, "gray");
	int row_index = 0;
	int column_index = 0;
	for(int i = 0; i< height; i++){
		for(int j=0; j<width; j++){
			int sum = 0;
			int variance = 0;
			for(int k =-2; k<3; k++){
				for(int l = -2; l<3; l++){
					if(i+k< 0 || i+k > (height-1) || j+l < 0 || j+l > (width-1)){
						if(i+k < 0){
							row_index = (i+k)*(-1)-1;
						}else if((i+k)>height -1){
							row_index = (height -1) - ((i+k) - (height - 1)) + 1;
						}else{
							row_index = i+k;
						}

						if(j+l <0){
							column_index = (-1) * (j+l) -1;
						}else if((j+l) > width -1){
							column_index = (width -1) -((j+l) - (width -1)) + 1;
						}else{
							column_index = j+l;
						}
						sum += imggray[row_index][column_index].getGrayscale();
					}else{
						sum+= imggray[i+k][j+l].getGrayscale();
					}
				}
			}
			sum = sum/25;
			variance = pow((imggray[i][j].getGrayscale() - sum),2);
			if(variance > 255){
				variance = 255;
			}else if(variance < 0){
				variance = 0;
			}
			Variance_map.imggray[i][j].setGrayscale(variance);
		}
	}

	return Variance_map;
}

Image Image::Edge_thinning(Image Sobel_edgemap_gx, Image Sobel_edgemap_gy){
	for(int i=0; i<height; i++){
		for(int j = 0; j<width; j++){
			if(Sobel_edgemap_gx.imggray[i][j].getGrayscale() > Sobel_edgemap_gy.imggray[i][j].getGrayscale()){
				//dominant direction is gx so we have to do the checking in the y direction
				if(i-1 <0 ||i+1 > 255){
					continue;
				}
				if((imggray[i][j].getGrayscale() > imggray[i-1][j].getGrayscale()) &&(imggray[i][j].getGrayscale() > imggray[i+1][j].getGrayscale())){
					imggray[i][j].setGrayscale(255);
				}else{
					imggray[i][j].setGrayscale(0);
				}
			}else{
				//dominant direction is gy
				if(j-1 <0 ||j+1 > 255){
					continue;
				}
				if((imggray[i][j].getGrayscale() > imggray[i][j-1].getGrayscale()) &&(imggray[i][j].getGrayscale() > imggray[i][j+1].getGrayscale())){
					imggray[i][j].setGrayscale(255);
				}else{
					imggray[i][j].setGrayscale(0);
				}
			}
		}
	}

	return *this;
}