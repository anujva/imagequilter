The Main file is the place from where all the code can be run. This is a visual studio 2012 project and can be easily opened in that.

In order to run the code please run one part at a time.. the code has been divided into problem 1 and problem 2 and problem 3. 

When you run the code for problem 1 please comment out the rest of the source since it will take a lot of time compiling and running. Same should be followed for problem 2 and problem 3. 

Currently the main file will run problem 3.

At the prompt you need to write the name of the file that you want to be recognized optically.

